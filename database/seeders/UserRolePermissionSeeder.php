<?php

namespace Database\Seeders;

use App\Constants\RolePermissionConstant;
use App\Constants\UserConstant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;

class UserRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('permissions')->delete();
        User::query()->firstOrCreate(
            ['id' => 1],
            ['name' => 'Owner', 'email' => 'owner@gmail.com', 'password' => bcrypt('12345678')]
        );

        $roles = [
            ['id' => 1, 'name' => 'Owner', 'guard_name' => 'web', 'created_at' => now(), 'updated_at' => now()],
            ['id' => 2, 'name' => 'User', 'guard_name' => 'web','created_at' => now(), 'updated_at' => now()],
        ];
        DB::table('roles')->insertOrIgnore($roles);

        $menus = [
            ['id' => 1, 'name' => RolePermissionConstant::MENU_DASHBOARD, 'guard_name' => 'web','parent_id' => 0, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 2, 'name' => RolePermissionConstant::MENU_USER,'guard_name' => 'web','parent_id' => 0, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 3, 'name' => RolePermissionConstant::MENU_REQUEST,'guard_name' => 'web','parent_id' => 0, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 4, 'name' => RolePermissionConstant::MENU_DEPARTMENT,'guard_name' => 'web', 'parent_id' => 0, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 5, 'name' => RolePermissionConstant::MENU_ROLE,'guard_name' => 'web', 'parent_id' => 0, 'created_at' => now(), 'updated_at' => now()],
        ];

        DB::table('permissions')->insertOrIgnore($menus);

        $permissions =[
            // Dashboard
            ['id' => 6, 'name' => RolePermissionConstant::PERMISSION_DASHBOARD_VIEW, 'guard_name' => 'web', 'parent_id' => 1, 'created_at' => now(), 'updated_at' => now()],

            //User
            ['id' => 7, 'name' => RolePermissionConstant::PERMISSION_USER_VIEW, 'guard_name' => 'web', 'parent_id' => 2, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 8, 'name' => RolePermissionConstant::PERMISSION_USER_CREATE, 'guard_name' => 'web', 'parent_id' => 2, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 9, 'name' => RolePermissionConstant::PERMISSION_USER_EDIT, 'guard_name' => 'web', 'parent_id' => 2, 'created_at' => now(), 'updated_at' => now()],

            // Request
            ['id' => 10, 'name' => RolePermissionConstant::PERMISSION_REQUEST_VIEW_ANY, 'guard_name' => 'web', 'parent_id' => 3, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 11, 'name' => RolePermissionConstant::PERMISSION_REQUEST_VIEW, 'guard_name' => 'web', 'parent_id' => 3, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 12, 'name' => RolePermissionConstant::PERMISSION_REQUEST_CREATE, 'guard_name' => 'web', 'parent_id' => 3, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 13, 'name' => RolePermissionConstant::PERMISSION_REQUEST_APPROVE, 'guard_name' => 'web', 'parent_id' => 3, 'created_at' => now(), 'updated_at' => now()],

            // Department
            ['id' => 14, 'name' => RolePermissionConstant::PERMISSION_DEPARTMENT_VIEW, 'guard_name' => 'web', 'parent_id' => 4, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 15, 'name' => RolePermissionConstant::PERMISSION_DEPARTMENT_CREATE, 'guard_name' => 'web', 'parent_id' => 4, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 16, 'name' => RolePermissionConstant::PERMISSION_DEPARTMENT_EDIT, 'guard_name' => 'web', 'parent_id' => 4, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 17, 'name' => RolePermissionConstant::PERMISSION_DEPERTMENT_DELETE,'guard_name' => 'web', 'parent_id' => 4, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 18, 'name' => RolePermissionConstant::PERMISION_ASSIGN_DEPARTMENT, 'guard_name' => 'web', 'parent_id' => 4, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 19, 'name' => RolePermissionConstant::PERMISSION_CREATE_WORKFLOW_DEPARTMENT, 'guard_name' => 'web', 'parent_id' => 4, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 20, 'name' => RolePermissionConstant::PERMISSION_EDIT_WORKFLOW_DEPARTMENT, 'guard_name' => 'web', 'parent_id' => 4, 'created_at' => now(), 'updated_at' => now()],

            // Role
            ['id' => 21, 'name' => RolePermissionConstant::PERMISSION_ROLE_VIEW, 'guard_name' => 'web', 'parent_id' => 5, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 22, 'name' => RolePermissionConstant::PERMISSION_ROLE_CREATE, 'guard_name' => 'web', 'parent_id' => 5, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 23, 'name' => RolePermissionConstant::PERMISSION_ROLE_EDIT, 'guard_name' => 'web', 'parent_id' => 5, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 24, 'name' => RolePermissionConstant::PERMISSION_ROLE_DELETE, 'guard_name' => 'web', 'parent_id' => 5, 'created_at' => now(), 'updated_at' => now()],
            ['id' => 25, 'name' => RolePermissionConstant::PERMISSION_CHANGE_PERMISSION, 'guard_name' => 'web', 'parent_id' => 5, 'created_at' => now(), 'updated_at' => now()],

        ];

        DB::table('permissions')->insertOrIgnore($permissions);

        // Assign permission to role
        $permission = Permission::query()->pluck('id')->toArray();
        $roleOwner = Role::query()->where('name', UserConstant::OWNER)->first();
        // Assign all permission to owner
        $roleOwner->hashPermission()->sync($permission);

    }
}
