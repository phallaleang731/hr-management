<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', [App\Http\Controllers\DashboardController::class, 'index'])->name('home');


Auth::routes();

Route::get('/logout', [App\Http\Controllers\HomeController::class, 'logout'])->name('logout');
Route::get('/changeLang/{lang}', [App\Http\Controllers\UserController::class, 'changeLang'])->name('changeLang');
// Role Prefix
Route::prefix('role')->group(function () {
    Route::get('/', [App\Http\Controllers\RoleController::class, 'index'])->name('role.index');
    Route::get('/create', [App\Http\Controllers\RoleController::class, 'create'])->name('role.create');
    Route::post('/', [App\Http\Controllers\RoleController::class, 'store'])->name('role.store');
    Route::get('/edit/{id}', [App\Http\Controllers\RoleController::class, 'edit'])->name('role.edit');
    Route::patch('/{id}', [App\Http\Controllers\RoleController::class, 'update'])->name('role.update');
    Route::get('role/delete/{id}', [App\Http\Controllers\RoleController::class, 'delete'])->name('role.delete');

    // Role Permission
    Route::get('/permission/{id}', [App\Http\Controllers\RoleController::class, 'rolePermission'])->name('role.permission');
    Route::post('/permission/store', [App\Http\Controllers\RoleController::class, 'storeRolePermission'])->name('role.permission.store');
});

// Department prefix
Route::prefix('department')->group(function () {
    Route::get('/', [App\Http\Controllers\DepartmentController::class, 'index'])->name('department.index');
    Route::get('/create', [App\Http\Controllers\DepartmentController::class, 'create'])->name('department.create');
    Route::post('/', [App\Http\Controllers\DepartmentController::class, 'store'])->name('department.store');
    Route::get('/edit/{id}', [App\Http\Controllers\DepartmentController::class, 'edit'])->name('department.edit');
    Route::patch('/{id}', [App\Http\Controllers\DepartmentController::class, 'update'])->name('department.update');
    Route::get('delete/{id}', [App\Http\Controllers\DepartmentController::class, 'destroy'])->name('department.delete');
    Route::get('/assign-department/{id}', [App\Http\Controllers\DepartmentController::class, 'assignDepartment'])->name('department.assign');
    Route::post('/assign-department/store', [App\Http\Controllers\DepartmentController::class, 'storeAssignDepartment'])->name('department.assign.store');
    Route::get('/assign-department/edit/{id}', [App\Http\Controllers\DepartmentController::class, 'editAssignDepartment'])->name('department.assign.edit');
    Route::patch('/assign-department/update/{id}', [App\Http\Controllers\DepartmentController::class, 'updateAssignDepartment'])->name('department.assign.update');
});
// User Prefix

Route::prefix('user')->group(function () {
    Route::get('/', [App\Http\Controllers\UserController::class, 'index'])->name('user.index');
    Route::get('/create', [App\Http\Controllers\UserController::class, 'create'])->name('user.create');
    Route::post('/', [App\Http\Controllers\UserController::class, 'store'])->name('user.store');
    Route::get('/edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('user.edit');
    Route::patch('/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('user.update');
    Route::delete('/{id}', [App\Http\Controllers\UserController::class, 'destroy'])->name('user.delete');
});

// Request
Route::prefix('request')->group(function () {
    Route::get('/', [App\Http\Controllers\RequestController::class, 'index'])->name('request.index');
    Route::get('/create', [App\Http\Controllers\RequestController::class, 'create'])->name('request.create');
    Route::post('/', [App\Http\Controllers\RequestController::class, 'store'])->name('request.store');
    Route::get('/edit/{id}', [App\Http\Controllers\RequestController::class, 'edit'])->name('request.edit');
    Route::patch('/{id}', [App\Http\Controllers\RequestController::class, 'update'])->name('request.update');
    Route::get('/user-department/{id}', [App\Http\Controllers\RequestController::class, 'getUserDepartment'])->name('request.user.department');
    Route::get('/approve/{id}', [App\Http\Controllers\RequestController::class, 'approve'])->name('request.approve');
    Route::get('/approve/store/{id}/{status}', [App\Http\Controllers\RequestController::class, 'storeApprove'])->name('request.approve.store');
    Route::get('/request/request-history/{id}', [App\Http\Controllers\RequestController::class, 'requestHistory'])->name('request.request.history');
});
