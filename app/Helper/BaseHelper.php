<?php

use App\Constants\UserConstant;
use App\Models\Permission;
use App\Models\RoleHasPermission;
use Illuminate\Support\Str;

if(!function_exists('isOwner')){
    function isOwner(){
        return auth()->user()->role_id == UserConstant::ROLE_OWNER;
    }
}

if(!function_exists('isUser')){
    function isUser(){
        return auth()->user()->role_id == UserConstant::ROLE_USER;
    }
}

if(!function_exists('authorize')){
    function authorize($permission){
       $permission = Permission::where('name', $permission)->first();
       if(empty($permission)){
           return false;
       }
       if(isOwner()){
           return true;
       }
       $rolePermission = RoleHasPermission::where('role_id', auth()->user()->role_id)->where('permission_id', $permission->id)->first();
         if(empty($rolePermission)){
              return false;
         }
        return true;
       
    }
}

if(!function_exists('authorizeMessage')){
    function authorizeMessage(){
            return  'You are not authorized to access this page';

    }
}

// format date time
if(!function_exists('formatDateTime')){
    function formatDateTime($date){
        return date('d-m-Y h:i:s', strtotime($date));
    }
}

// format date
if(!function_exists('formatDate')){
    function formatDate($date){
        return date('m-d-Y', strtotime($date));
    }
}

// limit description with 10 words
if(!function_exists('limitDescription')){
    function limitDescription($description){
        $words = explode(' ', $description);
        return implode(' ', array_slice($words, 0, 5));
    }
}

// calculate date diff
if(!function_exists('calculateDate')){
    function calculateDate($startDate, $endDate){
        $start = new DateTime($startDate);
        $end = new DateTime($endDate);
        $interval = $start->diff($end);
        return $interval->format('%a').' '.Str::plural('day',$interval->format('%a'));
    }
}

if (!function_exists('maskText')) {
    /**
     * Hidden privacy data, phone number, email, etc
     * @param mixed $payload
     * @return string
     */
    function maskText(mixed $payload): string
    {
        return $payload ? substr_replace((string)$payload, '***', 3, 3) : '';
    }
}



?>