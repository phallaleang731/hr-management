<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'language',
        'status',
        'photo'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    protected $appends = [
        'photo_url',
        'department_ids',
        'department_names',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function userDepartments()
    {
        return $this->belongsToMany(Department::class, 'user_departments', 'user_id', 'department_id')->orderBy('id', 'desc');
    }


    public function getPhotoUrlAttribute()
    {
        return $this->photo ? asset('uploads/users/' . $this->photo) : asset('uploads/users/avatar-place.png');
    }

    public function getRoleNameAttribute()
    {
        return $this->role->name ?? '';
    }
    public function getDepartmentIdsAttribute()
    {
        return $this->userDepartments->pluck('department_id')->toArray();
    }

    public function getDepartmentNamesAttribute()
    {
        return $this->userDepartments->pluck('name')->toArray();
    }


}
