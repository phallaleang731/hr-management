<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    
    protected $table = 'roles';

    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * Get the permissions for the role.
     */
    public function roleHasPermssions()
    {
        return $this->hasMany(RoleHasPermission::class);
    }

    public function hashPermission()
    {
        return $this->belongsToMany(Permission::class, 'role_has_permissions', 'role_id', 'permission_id');
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }


}
