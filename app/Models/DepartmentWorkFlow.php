<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepartmentWorkFlow extends Model
{
    use HasFactory;

    protected $table = 'department_workflows';

    protected $fillable = [
        'department_id',
        'role_id',
        'type',
        'step'
    ];

    protected $appends = ['department_name'];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

   public function getDepartmentNameAttribute()
    {
        return $this->department->name ?? '';
    }

    public function getCreatedAtAttribute($value)
    {
        return formatDateTime($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formatDateTime($value);
    }

    // approved_at
    public function getApprovedAtAttribute($value)
    {
        return formatDateTime($value);
    }
}
