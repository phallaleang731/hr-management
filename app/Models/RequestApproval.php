<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestApproval extends Model
{
    use HasFactory;

    protected $table = 'request_approvals';

    protected $fillable = [
        'request_id',
        'approver_id',
        'status',
        'comment',
        'approved_at',
        'role_id',
        'order_number'
    ];

    protected $appends = [
        'approve_by',
        'role_name',
        'department_name',
        'user_name',
        'request_type'
    ];
    public function approver()
    {
        return $this->belongsTo(User::class, 'approver_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function request()
    {
        return $this->belongsTo(Requests::class);
    }

    public function getApproveByAttribute()
    {
        return $this->approver->name ?? '';
    }

    public function getRoleNameAttribute()
    {
        return $this->role->name ?? '';
    }

    public function getDepartmentNameAttribute()
    {
        return $this->request->department_name?? '';
    }

    public function getUserNameAttribute()
    {
        return $this->request->user_name ?? '';
    }

    public function getRequestTypeAttribute()
    {
        return $this->request->type ?? '';
    }

    public function getCreatedAtAttribute($value)
    {
        return formatDateTime($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formatDateTime($value);
    }

    // approved_at
    public function getApprovedAtAttribute($value)
    {
        return formatDateTime($value);
    }

}
