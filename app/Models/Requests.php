<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
    use HasFactory;

    protected $table = 'requests';

    protected $fillable = [
        'user_id',
        'department_id',
        'type',
        'reason',
        'status',
        'start_date',
        'end_date',
    ];

    protected $appends = [
        'user_name',
        'department_name',
        'total_days',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function getUserNameAttribute()
    {
        return $this->user->name ?? '';
    }

    public function getDepartmentNameAttribute()
    {
        return $this->department->name ?? '';
    }

    public function requestAprroval()
    {
        return $this->hasMany(RequestApproval::class);
    }

    public function getCreatedAtAttribute($value)
    {
        return formatDateTime($value);
    }

    public function getStartDateAttribute($value)
    {
        return formatDate($value);
    }

    public function getEndDateAttribute($value)
    {
        return formatDate($value);
    }

    public function getTotalDaysAttribute()
    {
        return calculateDate($this->start_date, $this->end_date);
    }




}
