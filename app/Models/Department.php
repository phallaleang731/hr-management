<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    protected $table = 'departments';

    protected $fillable = [
        'name',
    ];

    public function departmentWorkFlows()
    {
        return $this->hasMany(DepartmentWorkFlow::class);
    }

    public function userDepartments()
    {
        return $this->hasMany(UserDepartment::class);
    }

    public function getCreatedAtAttribute($value)
    {
        return formatDateTime($value);
    }
}
