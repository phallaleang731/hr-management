<?php
 namespace App\Constants;

    class RequestConstant
    {
        const STATUS_PENDING = "Pending";
        const STATUS_APPROVED = "Approved";
        const STATUS_REJECTED = "Rejected";
    }
?>