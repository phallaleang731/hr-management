<?php
 namespace App\Constants;

    class RolePermissionConstant
    {
        // Menu Dashboard
        const MENU_DASHBOARD = "Dashboard";
        const PERMISSION_DASHBOARD_VIEW = "can view dashboard";

        // Menu User
        const MENU_USER = "User";
        const PERMISSION_USER_VIEW = "can view user";
        const PERMISSION_USER_CREATE = "can create user";
        const PERMISSION_USER_EDIT = "can edit user";

        // Menu Request
        const MENU_REQUEST = "Request";
        const PERMISSION_REQUEST_VIEW_ANY = "can view any request";
        const PERMISSION_REQUEST_VIEW = "can view request";
        const PERMISSION_REQUEST_CREATE = "can create request";
        const PERMISSION_REQUEST_APPROVE = "can approve request";

        // Menu Department
        const MENU_DEPARTMENT = "Department";
        const PERMISSION_DEPARTMENT_VIEW = "can view department";
        const PERMISSION_DEPARTMENT_CREATE = "can create department";
        const PERMISSION_DEPARTMENT_EDIT = "can edit department";
        const PERMISSION_DEPERTMENT_DELETE = "can delete department";
        const PERMISION_ASSIGN_DEPARTMENT = "can assign department";
        const PERMISSION_CREATE_WORKFLOW_DEPARTMENT = "can create workflow department";
        const PERMISSION_EDIT_WORKFLOW_DEPARTMENT = "can edit workflow department";

        // Menu Role
        const MENU_ROLE = "Role";
        const PERMISSION_ROLE_VIEW = "can view role";
        const PERMISSION_ROLE_CREATE = "can create role";
        const PERMISSION_ROLE_EDIT = "can edit role";
        const PERMISSION_ROLE_DELETE = "can delete role";
        const PERMISSION_CHANGE_PERMISSION= "can change permission";
    }
?>