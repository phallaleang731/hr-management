<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Constants\RolePermissionConstant;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(config('middleware.user'));
    }

     public function index()
    {
        if(!authorize(RolePermissionConstant::PERMISSION_USER_VIEW)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        $data['users'] = User::orderBy('id', 'desc')->paginate(10);
        $data['bc']   = [['link' => route('home'), 'page' => __('header.home')], ['link' => '#', 'page' => __('setting.user')]];
        return view('users.index', $data);
    }

    public function create()
    {
        if(!authorize(RolePermissionConstant::PERMISSION_USER_CREATE)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        $data['roles'] = Role::where('id', '!=', '1')->get();
        $data['departments'] = Department::all();
        $data['bc']   = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('user.index'), 'page' => __('setting.user')], ['link' => '#', 'page' => __('setting.add_user')]];
        return view('users.add', $data);
    
    }

    public function store(request $request)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_USER_CREATE)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        try{
            DB::beginTransaction();
            $request->validate([
                'username' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'password_confirmation' => 'required|same:password',
                'role_id' => 'required|exists:roles,id',
                'department_id' => 'required',
                'status' => 'required|in:0,1',
            ]);
            $data = [
                'name'         => $request->username,
                'email'        => $request->email,
                'password'     => bcrypt($request->password),
                'role_id'      => $request->role_id,
                'status'       => $request->status,
            ];
            if($request->photo){
                $file= $request->file('photo');
                $filename= $this->combineFileName($file->getClientOriginalName());
                $file-> move(public_path('uploads/users'), $filename);
                $data['photo'] = $filename;
            }
            $user = User::create($data);
            $user->userDepartments()->attach($request->department_id);
            if (!$user) {
                return redirect()->back()->with('error', __('setting.user_was_saved_failed'));
            }
            DB::commit();
            return redirect()->route('user.index')->with('success', __('setting.was_saved_successfully',['attr'=>'User']));
        }catch(Exception $exception){
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function edit($id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_USER_EDIT)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        $user = User::where('id', $id)->first();
        if(empty($user)){
            return redirect()->back()->with('error', __('setting.user_not_found'));
        }
        $data['user'] = $user;
        $data['roles'] = Role::where('id', '!=', '1')->get();
        $data['departments'] = Department::all();
        $data['bc'] = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('user.index'), 'page' => __('setting.user')], ['link' => '#', 'page' => __('setting.edit_user')]];
        return view('users.edit', $data);
    }

    public function update(Request $request, $id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_USER_EDIT)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        try{
            DB::beginTransaction();
            $request->validate([
                'username' => 'required',
                'email' => 'required|email|unique:users,email,'.$id.',id',
                'role_id' => 'required|exists:roles,id',
                'department_id' => 'required',
                'status' => 'required|in:0,1',
            ]);
            $user = User::find($id);
            if(empty($user)){
                return redirect()->back()->with('error', __('setting.user_not_found'));
            }
            $data = [
                'name'         => $request->username,
                'email'        => $request->email,
                'role_id'      => $request->role_id,
                'status'       => $request->status,
            ];
            if(!empty($request->password)){
                $data['password'] = bcrypt($request->password);
           }
            if(!empty($request->photo)){
                $file= $request->file('photo');
                $filename= $this->combineFileName($file->getClientOriginalName());
                $file-> move(public_path('uploads/users'), $filename);
                $data['photo'] = $filename;
            }
            $user->update($data);
            $user->userDepartments()->sync($request->department_id);
            DB::commit();
            return redirect()->route('user.index')->with('success', __('setting.was_updated_successfully',['attr'=>'User']));
        }catch(Exception $exception){
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
    public function changeLang($lang) {
        $data['language'] = $lang;
        User::where('id',Auth::user()->id)
            ->update($data);
        return redirect()->back();
    }

    public function combineFileName(string|int $fileName)
    {
        $newFileName = explode(' ', $fileName);
        $combineFileName = implode('_', $newFileName);
        return $combineFileName;
    }
    
}
