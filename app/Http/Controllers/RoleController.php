<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use App\Constants\RolePermissionConstant;
use App\Constants\UserConstant;
use App\Models\User;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware(config('middleware.user'));
    }

    public function index()
    {
        if(!authorize(RolePermissionConstant::PERMISSION_ROLE_VIEW)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        $data['roles'] = Role::where('name', '!=', UserConstant::OWNER)->orderBy('id', 'desc')->paginate(10);
        $data['bc']   = [['link' => route('home'), 'page' => __('header.home')], ['link' => '#', 'page' => __('setting.role')]];
        return view('roles.index', $data);
    }

    public function create()
    {
        if(!authorize(RolePermissionConstant::PERMISSION_ROLE_CREATE)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        $data['bc']   = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('role.index'), 'page' => __('setting.role')], ['link' => '#', 'page' => __('setting.add_role')]];
        return view('roles.add', $data);
    }
    public function store(request $request)
    {
      if(!authorize(RolePermissionConstant::PERMISSION_ROLE_CREATE)){
        return redirect()->back()->with('error', authorizeMessage());
      }
        $request->validate([
            'name' => 'required|unique:roles'
        ]);
      $data = [
        'name'         => $request->name,
        'description'  => $request->description
      ];
        $model = Role::create($data);
        if (!$model) {
            return redirect()->back()->with('error', __('setting.role_was_saved_failed'));
        }
      return redirect()->route('role.index')->with('success', __('setting.was_saved_successfully',['attr'=>'Role']));
    }
    public function edit($id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_ROLE_EDIT)){
            return redirect()->back()->with('error', authorizeMessage());
          }
      $role = Role::where('id', $id)->first();
        if(empty($role)){
            return redirect()->back()->with('error', __('setting.role_not_found'));
        }
      $data['role'] = $role;
      $data['bc'] = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('role.index'), 'page' => __('setting.role')], ['link' => '#', 'page' => __('setting.edit_role')]];
      return view('roles.edit', $data);
    }
    public function update(Request $request, $id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_ROLE_EDIT)){
            return redirect()->back()->with('error', authorizeMessage());
          }
      $request->validate([
        'name'  => 'required|unique:roles,name,'.$id.',id'
      ]);
       $role = Role::find($id);
       if(empty($role)){
         return redirect()->back()->with('error', __('setting.role_not_found'));
       }
       if ($role->name == 'Admin') {
         return redirect()->back()->with('error', __('setting.role_can_not_update'));
       }
        $role->name = $request->name;
        $role->description = $request->description;
        $role->save();
        return redirect()->route('role.index')->with('success', __('setting.was_updated_successfully',['attr'=>'Role']));
    }
    public function delete($id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_ROLE_DELETE)){
            return redirect()->back()->with('error', authorizeMessage());
          }
      $countRole = User::where('role_id', $id)
        ->count();
      if ($countRole > 0) {
        return redirect()->back()->with('error', __('setting.role_can_not_delete_because_it_have_user'));
      }
      $role = Role::where('id', $id)
        ->delete();
  
      if (!empty($role)) {
        return redirect()->back()->with('success', __('setting.role_was_deleted_successfully'));
      }
      return redirect()->back()->with('error', __('setting.role_was_delete_failed'));
    }

    public function rolePermission($id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_CHANGE_PERMISSION)){
            return redirect()->back()->with('error', authorizeMessage());
          }
        $role = Role::where('id', $id)->first();
        if(empty($role)){
            return redirect()->back()->with('error', __('setting.role_not_found'));
        }
        $data['role'] = $role;
        $data['permissions'] = Permission::with('children')->where('parent_id', 0)->get();
        $data['rolePermissions'] = $role->roleHasPermssions->pluck('permission_id')->toArray();
        $data['bc'] = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('role.index'), 'page' => __('setting.role')], ['link' => '#', 'page' => __('setting.role_permission')]];
        return view('roles.permission', $data);
    }

    public function storeRolePermission(Request $request)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_CHANGE_PERMISSION)){
            return redirect()->back()->with('error', authorizeMessage());
          }
        $request->validate([
            'role_id' => 'required',
            'permissions' => 'required'
        ]);
        $role = Role::find($request->role_id);
        if(empty($role)){
            return redirect()->back()->with('error', __('setting.role_not_found'));
        }
        $role->roleHasPermssions()->delete();
        $permissions = $request->permissions;
        foreach ($permissions as $permission) {
            $role->roleHasPermssions()->create(['permission_id' => $permission]);
        }
        return redirect()->route('role.index')->with('success', __('setting.was_saved_successfully',['attr'=>'Role Permission']));
    }
}
