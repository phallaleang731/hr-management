<?php

namespace App\Http\Controllers;

use App\Constants\RequestConstant;
use Illuminate\Http\Request;
use App\Models\Requests;
use App\Models\User;
use App\Constants\UserConstant;
use Illuminate\Support\Facades\Auth;
use App\Constants\RolePermissionConstant;
use App\Models\DepartmentWorkFlow;
use App\Models\RequestApproval;
use Illuminate\Support\Facades\DB;
use Exception;

class RequestController extends Controller
{
    public function __construct()
    {
        $this->middleware(config('middleware.user'));
    }

    public function index()
    {
        if(!authorize(RolePermissionConstant::PERMISSION_REQUEST_VIEW)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        $requests = Requests::query();
        if(isUser()){
            $requests->where('user_id', Auth::user()->id);
        }
        $data['requests'] = $requests->orderBy('id', 'desc')->paginate(10);
        $data['bc']   = [['link' => route('home'), 'page' => __('header.home')], ['link' => '#', 'page' => __('setting.request')]];
        return view('requests.index', $data);
    }

    public function create()
    {
        if(!authorize(RolePermissionConstant::PERMISSION_REQUEST_CREATE)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        $data['users'] = User::where('role_id', '!=', UserConstant::ROLE_OWNER)->orderBy('id', 'desc')->get();
        if(Auth::user()->role_id != UserConstant::ROLE_OWNER){
            $userDepartments= Auth::user()->userDepartments;
            $data['departments'] = $userDepartments;
        }
        $data['bc']   = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('request.index'), 'page' => __('setting.request')], ['link' => '#', 'page' => __('setting.add_request')]];
        return view('requests.add', $data);
    }

    public function getUserDepartment($id)
    {
        $user = User::find($id);
        if(empty($user)){
            return response()->json(['error' => __('setting.user_not_found')]);
        }
        return response()->json(['user_department' => $user->userDepartments]);
    }

    public function store(request $request)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_REQUEST_CREATE)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        try{
            DB::beginTransaction();
            $request->validate([
                'user_id' => 'required',
                'department_id' => 'required',
                'start_date' => 'required|date|before:end_date',
                'end_date' => 'required|date|after:start_date',
                'reason' => 'required'
            ]);
            $startDate = date('Y-m-d H:i:s', strtotime($request->start_date));
            $endDate = date('Y-m-d H:i:s', strtotime($request->end_date));
            $user = User::find($request->user_id);

            if(!$user) {
                return redirect()->back()->with('error', __('setting.user_not_found'));
            }
            $userDepartmentsIds = $user->userDepartments->pluck('id')->toArray();
            if(!in_array($request->department_id, $userDepartmentsIds)) {
                return redirect()->back()->with('error', __('setting.department_not_belong_to_user'));
            }
            $data = [
                'user_id'       => $request->user_id ?? Auth::user()->id,
                'department_id' => $request->department_id,
                'type'         => $request->type,
                'reason'        => $request->reason,
                'status'        => RequestConstant::STATUS_PENDING,
                'start_date'    => $startDate,
                'end_date'      => $endDate,
                'created_at'    => date('Y-m-d H:i:s'),
            ];
            $request = Requests::create($data);
            if (!$request) {
                return redirect()->back()->with('error', __('setting.request_was_saved_failed'));
            }
            $userDepartment = DepartmentWorkFlow::query()
                ->where('department_id', $request->department_id)
                ->where('type', $request->type)
                ->first();
            if(empty($userDepartment)){
                return redirect()->back()->with('error', __('setting.department_not_belong_to_user'));
            }
            $steps = json_decode($userDepartment->step);
            $data = [];
            // create only first step and one record in request_approval table
            $data = [
                'request_id' => $request->id,
                'role_id' => $steps[0]->role_id,
                'status' => RequestConstant::STATUS_PENDING,
                'order_number' => $steps[0]->order_number
            ];
            RequestApproval::create($data);
            DB::commit();
            return redirect()->route('request.index')->with('success', __('setting.was_saved_successfully',['attr'=>'Request']));
        }catch(Exception $exception){
            DB::rollBack();
            return redirect()->back()->with('error',$exception->getMessage());
        }
    }

    public function storeApprove($id, $status)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_REQUEST_APPROVE)){
            return redirect()->back()->with('error', authorizeMessage());
         }
            $request = Requests::find($id);
            if(empty($request)){
                return redirect()->back()->with('error', __('setting.request_not_found'));
            }
            // Find the workflow for the request
            $userDepartment = DepartmentWorkFlow::where('department_id', $request->department_id)
            ->where('type', $request->type)
            ->first();
            if (empty($userDepartment)) {
            return redirect()->back()->with('error', __('setting.department_not_belong_to_user'));
            }
            $steps = json_decode($userDepartment->step);
            // Check if the authenticated user is authorized to approve this request
            $roleIds = array_column($steps, 'role_id');
            if (!in_array(Auth::user()->role_id, $roleIds)) {
                return redirect()->back()->with('error', __('setting.you_are_not_authorized_to_approve_this_request'));
            }
            $requestApprovals = RequestApproval::where('request_id', $id)->get();
            if(!empty($requestApprovals)){
                // check if the requestApprovals has status rejected can not approve and return error
                $rejected = array_filter($requestApprovals->toArray(), function($requestApproval){
                    return $requestApproval['status'] == RequestConstant::STATUS_REJECTED;
                });
                if(count($rejected) > 0){
                    return redirect()->back()->with('error', __('setting.request_was_rejected'));
                }
            }
           
            
             // Find the request approval for the current user's role
                $requestApproval = RequestApproval::where('request_id', $id)
                ->where('role_id', Auth::user()->role_id)
                ->first();
            if (empty($requestApproval)) {
                return redirect()->back()->with('error', __('setting.waiting_for_other_role_to_approve'));
            }
           
            // how to check step order_number in $steps with current current role_id if not equal return error
            if(!empty($requestApproval)){
                // Update the status of the current request approval
                if ($requestApproval->status != RequestConstant::STATUS_PENDING) {
                    return redirect()->back()->with('error', __('setting.request_was_already_checked',['attr'=>$requestApproval->status]));
                }
            }
            // update status in request_approval table
            $requestApproval->status = $status;
            $requestApproval->approver_id = Auth::user()->id;
            $requestApproval->approved_at = date('Y-m-d H:i:s');
            $requestApproval->save();
        // Create new record in request_approval table with next role_id and order_number
            $nextStep = array_filter($steps, function($step) use ($requestApproval){
                return $step->order_number == $requestApproval->order_number + 1;
            });
            foreach($nextStep as $step){
                $nextStep = $step;
            }
            $data = [
                'request_id' => $id,
                'role_id' => $nextStep->role_id ?? 0,
                'status' => RequestConstant::STATUS_PENDING,
                'order_number' => $nextStep->order_number ?? 0
            ];
            if($status == RequestConstant::STATUS_APPROVED){
                if(count($requestApprovals) == count( $roleIds)){
                    $request->status = RequestConstant::STATUS_APPROVED;
                    $request->save();
                }else{
                    RequestApproval::create($data);
                }
            }
            if($status == RequestConstant::STATUS_REJECTED){
                $request->status = RequestConstant::STATUS_REJECTED;
                $request->save();
            }

        return redirect()->route('request.index')->with('success', __('setting.request_was_approved'));
    }

    public function requestHistory($requestId)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_REQUEST_VIEW_ANY)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        $data['requestApprovals'] = RequestApproval::where('request_id', $requestId)->paginate(10);
        $data['bc']   = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('request.index'), 'page' => __('setting.request')], ['link' => '#', 'page' => __('setting.request_history')]];
        return view('requests.request_history', $data);
    }


}
