<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware(config('middleware.user'));
    }
    
    public function index()
    {
        $data['bc']   = [['link' => route('home'), 'page' => __('header.home')]];
        return view('dashboard', $data);
    }
}
