<?php

namespace App\Http\Controllers;

use App\Constants\RolePermissionConstant;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\DepartmentWorkFlow;
use App\Models\Requests;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware(config('middleware.user'));
    }

    public function index()
    {
        if(!authorize(RolePermissionConstant::PERMISSION_DEPARTMENT_VIEW)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        $data['departments'] = Department::orderBy('id', 'desc')->paginate(10);
        $data['bc']   = [['link' => route('home'), 'page' => __('header.home')], ['link' => '#', 'page' => __('setting.department')]];
        return view('departments.index', $data);
    }

    public function create()
    {
        if(!authorize(RolePermissionConstant::PERMISSION_DEPARTMENT_CREATE)){
            return redirect()->back()->with('error', authorizeMessage());
         }
        $data['bc']   = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('department.index'), 'page' => __('setting.department')], ['link' => '#', 'page' => __('setting.add_department')]];
        return view('departments.add', $data);
    }

    public function store(request $request)
    {
      if(!authorize(RolePermissionConstant::PERMISSION_DEPARTMENT_CREATE)){
        return redirect()->back()->with('error', authorizeMessage());
      }
      $request->validate([
        'name' => 'required|unique:departments'
      ]);
      $data['name'] = $request->name;
        $model = Department::create($data);
        if (!$model) {
            return redirect()->back()->with('error', __('setting.was_saved_failed',['attr'=>'Department']));
        }
      return redirect()->route('department.index')->with('success', __('setting.was_saved_successfully',['attr'=>'Department']));
    }
    public function edit($id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_DEPARTMENT_EDIT)){
            return redirect()->back()->with('error', authorizeMessage());
          }
        $department = Department::where('id', $id)->first();
        if (!$department) {
            return redirect()->back()->with('error', __('setting.department_not_found'));
        }
        $data['department'] = $department;
        $data['bc'] = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('role.index'), 'page' => __('setting.department')], ['link' => '#', 'page' => __('setting.edit_department')]];
        return view('departments.edit', $data);
    }
    public function update(Request $request, $id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_DEPARTMENT_EDIT)){
            return redirect()->back()->with('error', authorizeMessage());
          }
      $request->validate([
        'name'  => 'required|unique:departments,name,'.$id.',id'
      ]);
      $data = [
        'name' => $request->name,
      ];
      $model =Department::where('id', $id)->update($data);
      if (!$model) {
        return redirect()->back()->with('error', __('setting.was_saved_failed',['attr'=>'Department']));
      }
      return redirect()->route('department.index')->with('success', __('setting.was_updated_successfully',['attr'=>'Department']));
    }

    public function destroy($id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_DEPERTMENT_DELETE)){
            return redirect()->back()->with('error', authorizeMessage());
          }
        $department = Department::where('id', $id)->first();
        if (!$department) {
            return redirect()->back()->with('error', __('setting.department_not_found'));
        }
        // check if department_id already used in user_department table can not delete
        $userDepartments = $department->userDepartments;
        if ($userDepartments->count() > 0) {
            return redirect()->back()->with('error', __('setting.department_already_used'));
        }
        if(!$department->delete()){
            return redirect()->back()->with('error', __('setting.department_deleted_failed'));
        }
        return redirect()->back()->with('success', __('setting.department_deleted_successfully'));
    }

    public function assignDepartment($id)
    {
        if(!authorize(RolePermissionConstant::PERMISION_ASSIGN_DEPARTMENT)){
            return redirect()->back()->with('error', authorizeMessage());
          }
        $department = Department::where('id', $id)->first();
        if (!$department) {
            return redirect()->back()->with('error', __('setting.department_not_found'));
        }
        $data['department'] = $department;
        $data['departmentWorkFlows'] = $department->departmentWorkFlows;
        $data['roles'] = Role::where('id', '!=', '1')->orderBy('name', 'asc')->get();
        $data['bc'] = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('department.index'), 'page' => __('setting.department')], ['link' => '#', 'page' => __('setting.assign_department')]];
        return view('departments.assign_department', $data);
    }

    public function storeAssignDepartment(Request $request)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_CREATE_WORKFLOW_DEPARTMENT)){
            return redirect()->back()->with('error', authorizeMessage());
          }
        $request->validate([
            'type' => 'required|in:Mission,Leave',
        ]);
        // check if department_id already assigned with both type of workflow show error
        $departmentWorkFlow = DepartmentWorkFlow::where('department_id', $request->department_id)->where('type', $request->type)->first();
        if ($departmentWorkFlow) {
            return redirect()->back()->with('error', __('setting.department_already_assigned_with',['type'=>$request->type]));
        }
        $step = $request->step;
        $data = [];
        foreach ($step as $key => $value) {
            $data[] = [
                'role_id' => $value,
                'order_number' => $key+1
            ];
        }
        $departmentWorkFlow = new DepartmentWorkFlow();
        $departmentWorkFlow->department_id = $request->department_id;
        $departmentWorkFlow->type = $request->type;
        $departmentWorkFlow->step = json_encode($data);
        $departmentWorkFlow->save();
        return redirect()->back()->with('success', __('setting.department_assigned_successfully'));
    }

    public function editAssignDepartment($id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_EDIT_WORKFLOW_DEPARTMENT)){
            return redirect()->back()->with('error', authorizeMessage());
          }
        // check if step in request_aprroval still pending or not allow to edit
        $departmentWorkFlow = DepartmentWorkFlow::where('id', $id)->first();
        if (!$departmentWorkFlow) {
            return redirect()->back()->with('error', __('setting.department_not_found'));
        }
        $requestApprovals = Requests::query()
            ->where('department_id', $departmentWorkFlow->department_id)
            ->where('type', $departmentWorkFlow->type)
            ->where('status', 'Pending')
            ->count();
        if ($requestApprovals > 0) {
            return redirect()->back()->with('error', __('setting.can_not_to_edit_because_request_is_pending'));
        }
        
        $data['departmentWorkFlows'] = $departmentWorkFlow;
        $data['roles'] = Role::where('id', '!=', '1')->orderBy('name', 'asc')->get();
        $data['bc'] = [['link' => route('home'), 'page' => __('header.home')], ['link' => route('department.index'), 'page' => __('setting.department')], ['link' => '#', 'page' => __('setting.assign_department')]];
        return view('departments.edit_assign_department', $data);
    }

    public function updateAssignDepartment(Request $request, $id)
    {
        if(!authorize(RolePermissionConstant::PERMISSION_EDIT_WORKFLOW_DEPARTMENT)){
            return redirect()->back()->with('error', authorizeMessage());
          }
        $departmentWorkFlow = DepartmentWorkFlow::where('id', $id)->first();
        if (!$departmentWorkFlow) {
            return redirect()->back()->with('error', __('setting.department_not_found'));
        }
        $step = $request->step;
        $data = [];
        foreach ($step as $key => $value) {
            $data[] = [
                'role_id' => $value,
                'order_number' => $key+1
            ];
        }
        $departmentWorkFlow->department_id = $request->department_id;
        $departmentWorkFlow->step = json_encode($data);
        $departmentWorkFlow->save();
        return redirect()->route('department.assign',$departmentWorkFlow->department_id)->with('success', __('setting.department_assigned_successfully'));
    }
}
