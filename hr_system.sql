-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: May 07, 2024 at 04:49 PM
-- Server version: 5.7.39
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'IT', NULL, '2024-05-05 02:36:42'),
(2, 'IT Department', '2024-05-02 05:01:58', '2024-05-02 05:01:58'),
(3, 'Sale Department', '2024-05-02 05:03:20', '2024-05-07 15:35:58');

-- --------------------------------------------------------

--
-- Table structure for table `department_workflows`
--

CREATE TABLE `department_workflows` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `type` enum('Mission','Leave') NOT NULL,
  `step` json NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department_workflows`
--

INSERT INTO `department_workflows` (`id`, `department_id`, `type`, `step`, `created_at`, `updated_at`) VALUES
(2, 1, 'Leave', '[{\"role_id\": \"5\", \"order_number\": 1}, {\"role_id\": \"6\", \"order_number\": 2}, {\"role_id\": \"4\", \"order_number\": 3}, {\"role_id\": \"3\", \"order_number\": 4}]', '2024-05-03 09:12:12', '2024-05-05 21:40:39'),
(3, 1, 'Mission', '[{\"role_id\": \"6\", \"order_number\": 1}, {\"role_id\": \"3\", \"order_number\": 2}]', '2024-05-03 09:16:13', '2024-05-05 21:40:45'),
(4, 2, 'Leave', '[{\"role_id\": \"3\", \"order_number\": 1}, {\"role_id\": \"4\", \"order_number\": 2}]', '2024-05-03 09:37:48', '2024-05-06 16:42:13'),
(5, 2, 'Mission', '[{\"role_id\": \"3\", \"order_number\": 1}, {\"role_id\": \"5\", \"order_number\": 2}]', '2024-05-03 09:38:47', '2024-05-06 16:42:31'),
(6, 3, 'Leave', '[{\"role_id\": \"3\", \"order_number\": 1}, {\"role_id\": \"6\", \"order_number\": 2}, {\"role_id\": \"4\", \"order_number\": 3}]', '2024-05-03 09:39:58', '2024-05-06 16:43:12'),
(7, 3, 'Mission', '[{\"role_id\": \"3\", \"order_number\": 1}, {\"role_id\": \"6\", \"order_number\": 2}, {\"role_id\": \"4\", \"order_number\": 3}, {\"role_id\": \"5\", \"order_number\": 4}]', '2024-05-03 09:41:18', '2024-05-06 16:43:47'),
(8, 7, 'Leave', '[{\"role_id\": \"5\", \"order_number\": 1}, {\"role_id\": \"6\", \"order_number\": 2}]', '2024-05-05 21:39:21', '2024-05-05 21:39:21'),
(9, 7, 'Mission', '[{\"role_id\": \"4\", \"order_number\": 1}]', '2024-05-05 21:40:11', '2024-05-05 21:40:11'),
(10, 8, 'Leave', '[{\"role_id\": \"3\", \"order_number\": 1}, {\"role_id\": \"4\", \"order_number\": 2}, {\"role_id\": \"5\", \"order_number\": 3}, {\"role_id\": \"6\", \"order_number\": 4}]', '2024-05-06 16:00:21', '2024-05-06 16:00:21'),
(11, 8, 'Mission', '[{\"role_id\": \"4\", \"order_number\": 1}, {\"role_id\": \"3\", \"order_number\": 2}, {\"role_id\": \"5\", \"order_number\": 3}, {\"role_id\": \"6\", \"order_number\": 4}]', '2024-05-06 16:00:43', '2024-05-06 16:28:57');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2024_05_01_161046_create_departments_table', 1),
(7, '2024_05_01_161059_create_roles_table', 1),
(8, '2024_05_01_161120_create_requests_table', 1),
(9, '2024_05_01_162209_create_sessions_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `parent_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 0, 'Dashboard', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(2, 0, 'User', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(3, 0, 'Request', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(4, 0, 'Department', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(5, 0, 'Role', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(6, 1, 'can view dashboard', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(7, 2, 'can view user', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(8, 2, 'can create user', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(9, 2, 'can edit user', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(10, 3, 'can view any request', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(11, 3, 'can view request', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(12, 3, 'can create request', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(13, 3, 'can approve request', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(14, 4, 'can view department', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(15, 4, 'can create department', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(16, 4, 'can edit department', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(17, 4, 'can delete department', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(18, 4, 'can assign department', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(19, 4, 'can create workflow department', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(20, 4, 'can edit workflow department', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(21, 5, 'can view role', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(22, 5, 'can create role', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(23, 5, 'can edit role', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(24, 5, 'can delete role', '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(25, 5, 'can change permission', '2024-05-06 16:52:28', '2024-05-06 16:52:28');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `type` enum('Leave','Mission') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Pending','Approved','Rejected') COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `user_id`, `department_id`, `type`, `status`, `reason`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'Leave', 'Approved', '<p>Personal busy</p>', '2024-07-04 17:00:00', '2024-08-04 17:00:00', '2024-05-06 17:10:30', '2024-05-06 17:11:23'),
(2, 1, 3, 'Mission', 'Approved', 'ចុះខេត្ត', '2024-07-04 17:00:00', '2024-08-04 17:00:00', '2024-05-07 01:11:45', '2024-05-07 01:14:05'),
(3, 1, 2, 'Mission', 'Approved', '<p>ចុះតាមខេត្តកណ្ដាល</p>', '2024-07-04 17:00:00', '2024-08-04 17:00:00', '2024-05-07 01:18:48', '2024-05-07 01:19:45'),
(4, 1, 3, 'Leave', 'Approved', '<p>មូលហេតុរវល់ផ្ទាស់ខ្លួន</p>', '2024-07-04 17:00:00', '2024-08-04 17:00:00', '2024-05-07 01:20:38', '2024-05-07 01:22:29'),
(5, 10, 2, 'Leave', 'Approved', '<p>Persional busy</p>', '2024-07-04 17:00:00', '2024-08-04 17:00:00', '2024-05-07 02:36:53', '2024-05-07 02:47:45'),
(6, 6, 2, 'Leave', 'Approved', '<p>Personal Busy</p>', '2024-07-04 17:00:00', '2024-08-04 17:00:00', '2024-05-07 02:49:58', '2024-05-07 02:50:44'),
(7, 6, 2, 'Mission', 'Rejected', '<p>ចុះតាមខេត្ត</p>', '2024-07-04 17:00:00', '2024-09-04 17:00:00', '2024-05-07 03:06:11', '2024-05-07 03:06:55'),
(8, 10, 2, 'Leave', 'Approved', '<p>Personal busy</p>', '2024-07-04 17:00:00', '2024-10-04 17:00:00', '2024-05-07 03:38:36', '2024-05-07 04:01:08'),
(9, 1, 2, 'Leave', 'Approved', '<p>Personal busy</p>', '2024-07-04 17:00:00', '2024-08-04 17:00:00', '2024-05-07 07:39:55', '2024-05-07 08:33:42'),
(10, 1, 2, 'Mission', 'Approved', '<p>Mission province</p>', '2024-07-04 17:00:00', '2024-08-04 17:00:00', '2024-05-07 08:21:32', '2024-05-07 08:35:22'),
(11, 10, 2, 'Leave', 'Approved', '<p>Personal busy</p>', '2024-07-04 17:00:00', '2024-08-04 17:00:00', '2024-05-07 08:58:45', '2024-05-07 09:23:26'),
(12, 1, 3, 'Mission', 'Approved', '<p>ចុះបេសកម្មតាមខេត្ត</p>', '2024-07-04 17:00:00', '2024-09-04 17:00:00', '2024-05-07 15:35:41', '2024-05-07 15:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `request_approvals`
--

CREATE TABLE `request_approvals` (
  `id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `status` enum('Pending','Approved','Rejected') NOT NULL,
  `approved_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment` varchar(255) DEFAULT NULL,
  `role_id` int(11) NOT NULL DEFAULT '0',
  `order_number` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `request_approvals`
--

INSERT INTO `request_approvals` (`id`, `request_id`, `approver_id`, `status`, `approved_at`, `comment`, `role_id`, `order_number`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 'Approved', '2024-05-06 17:11:05', NULL, 3, 1, '2024-05-06 17:10:30', '2024-05-06 17:11:05'),
(2, 1, 6, 'Approved', '2024-05-06 17:11:23', NULL, 4, 2, '2024-05-06 17:11:05', '2024-05-06 17:11:23'),
(3, 2, 4, 'Approved', '2024-05-07 01:12:36', NULL, 3, 1, '2024-05-07 01:11:45', '2024-05-07 01:12:36'),
(4, 2, 8, 'Approved', '2024-05-07 01:13:11', NULL, 6, 2, '2024-05-07 01:12:36', '2024-05-07 01:13:11'),
(5, 2, 6, 'Approved', '2024-05-07 01:13:41', NULL, 4, 3, '2024-05-07 01:13:11', '2024-05-07 01:13:41'),
(6, 2, 7, 'Approved', '2024-05-07 01:14:05', NULL, 5, 4, '2024-05-07 01:13:41', '2024-05-07 01:14:05'),
(7, 3, 4, 'Approved', '2024-05-07 01:19:16', NULL, 3, 1, '2024-05-07 01:18:48', '2024-05-07 01:19:16'),
(8, 3, 7, 'Approved', '2024-05-07 01:19:45', NULL, 5, 2, '2024-05-07 01:19:16', '2024-05-07 01:19:45'),
(9, 4, 4, 'Approved', '2024-05-07 01:21:29', NULL, 3, 1, '2024-05-07 01:20:38', '2024-05-07 01:21:29'),
(10, 4, 8, 'Approved', '2024-05-07 01:22:01', NULL, 6, 2, '2024-05-07 01:21:29', '2024-05-07 01:22:01'),
(11, 4, 6, 'Approved', '2024-05-07 01:22:29', NULL, 4, 3, '2024-05-07 01:22:01', '2024-05-07 01:22:29'),
(12, 5, 4, 'Approved', '2024-05-07 02:47:29', NULL, 3, 1, '2024-05-07 02:36:53', '2024-05-07 02:47:29'),
(13, 5, 6, 'Approved', '2024-05-07 02:47:45', NULL, 4, 2, '2024-05-07 02:47:29', '2024-05-07 02:47:45'),
(14, 6, 4, 'Approved', '2024-05-07 02:50:28', NULL, 3, 1, '2024-05-07 02:49:58', '2024-05-07 02:50:28'),
(15, 6, 6, 'Approved', '2024-05-07 02:50:44', NULL, 4, 2, '2024-05-07 02:50:28', '2024-05-07 02:50:44'),
(16, 7, 4, 'Rejected', '2024-05-07 03:06:55', NULL, 3, 1, '2024-05-07 03:06:11', '2024-05-07 03:06:55'),
(17, 8, 4, 'Approved', '2024-05-07 03:38:55', NULL, 3, 1, '2024-05-07 03:38:36', '2024-05-07 03:38:55'),
(18, 8, 6, 'Approved', '2024-05-07 04:01:08', NULL, 4, 2, '2024-05-07 03:38:55', '2024-05-07 04:01:08'),
(19, 9, 4, 'Approved', '2024-05-07 08:32:46', NULL, 3, 1, '2024-05-07 07:39:55', '2024-05-07 08:32:46'),
(20, 10, 4, 'Approved', '2024-05-07 08:32:43', NULL, 3, 1, '2024-05-07 08:21:32', '2024-05-07 08:32:43'),
(21, 10, 7, 'Approved', '2024-05-07 08:35:22', NULL, 5, 2, '2024-05-07 08:32:43', '2024-05-07 08:35:22'),
(22, 9, 6, 'Approved', '2024-05-07 08:33:42', NULL, 4, 2, '2024-05-07 08:32:46', '2024-05-07 08:33:42'),
(23, 11, 4, 'Approved', '2024-05-07 09:23:11', NULL, 3, 1, '2024-05-07 08:58:45', '2024-05-07 09:23:11'),
(24, 11, 6, 'Approved', '2024-05-07 09:23:26', NULL, 4, 2, '2024-05-07 09:23:11', '2024-05-07 09:23:26'),
(25, 12, 4, 'Approved', '2024-05-07 15:37:49', NULL, 3, 1, '2024-05-07 15:35:41', '2024-05-07 15:37:49'),
(26, 12, 8, 'Approved', '2024-05-07 15:38:20', NULL, 6, 2, '2024-05-07 15:37:49', '2024-05-07 15:38:20'),
(27, 12, 6, 'Approved', '2024-05-07 15:38:53', NULL, 4, 3, '2024-05-07 15:38:20', '2024-05-07 15:38:53'),
(28, 12, 7, 'Approved', '2024-05-07 15:39:29', NULL, 5, 4, '2024-05-07 15:38:53', '2024-05-07 15:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Owner', 'Can mange whole system', '2024-05-02 02:56:56', '2024-05-03 07:55:03'),
(2, 'User', '', '2024-05-02 04:13:00', '2024-05-02 04:32:37'),
(3, 'Team Leader', 'Team leader', '2024-05-03 07:50:43', '2024-05-07 15:32:57'),
(4, 'HR Manager', 'Human Resource Manager&nbsp;', '2024-05-03 07:52:06', '2024-05-03 07:54:34'),
(5, 'CEO', '<p>Chief Executive Officer</p>', '2024-05-03 07:53:35', '2024-05-03 07:54:46'),
(6, 'CFO', '<p>Chief Finance Officer</p>', '2024-05-03 07:54:12', '2024-05-03 07:54:12'),
(7, 'System admin', '<p>Manage system</p>', '2024-05-04 01:06:49', '2024-05-04 01:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`id`, `role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(84, 2, 6, '2024-05-05 03:55:02', '2024-05-05 03:55:02'),
(85, 2, 11, '2024-05-05 03:55:02', '2024-05-05 03:55:02'),
(86, 2, 12, '2024-05-05 03:55:02', '2024-05-05 03:55:02'),
(87, 1, 6, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(88, 1, 7, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(89, 1, 8, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(90, 1, 9, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(91, 1, 10, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(92, 1, 11, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(93, 1, 12, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(94, 1, 13, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(95, 1, 14, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(96, 1, 15, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(97, 1, 16, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(98, 1, 17, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(99, 1, 18, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(100, 1, 19, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(101, 1, 20, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(102, 1, 21, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(103, 1, 22, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(104, 1, 23, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(105, 1, 24, '2024-05-05 04:01:07', '2024-05-05 04:01:07'),
(123, 3, 10, '2024-05-05 08:30:23', '2024-05-05 08:30:23'),
(124, 3, 11, '2024-05-05 08:30:23', '2024-05-05 08:30:23'),
(125, 3, 13, '2024-05-05 08:30:23', '2024-05-05 08:30:23'),
(145, 6, 10, '2024-05-05 23:52:39', '2024-05-05 23:52:39'),
(146, 6, 11, '2024-05-05 23:52:39', '2024-05-05 23:52:39'),
(147, 6, 13, '2024-05-05 23:52:39', '2024-05-05 23:52:39'),
(163, 1, 1, '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(164, 1, 2, '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(165, 1, 3, '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(166, 1, 4, '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(167, 1, 5, '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(168, 1, 25, '2024-05-06 16:52:28', '2024-05-06 16:52:28'),
(169, 5, 6, '2024-05-07 01:15:38', '2024-05-07 01:15:38'),
(170, 5, 7, '2024-05-07 01:15:38', '2024-05-07 01:15:38'),
(171, 5, 10, '2024-05-07 01:15:38', '2024-05-07 01:15:38'),
(172, 5, 11, '2024-05-07 01:15:38', '2024-05-07 01:15:38'),
(173, 5, 13, '2024-05-07 01:15:38', '2024-05-07 01:15:38'),
(181, 4, 6, '2024-05-07 07:35:52', '2024-05-07 07:35:52'),
(182, 4, 10, '2024-05-07 07:35:52', '2024-05-07 07:35:52'),
(183, 4, 11, '2024-05-07 07:35:52', '2024-05-07 07:35:52'),
(184, 4, 12, '2024-05-07 07:35:52', '2024-05-07 07:35:52'),
(185, 4, 13, '2024-05-07 07:35:52', '2024-05-07 07:35:52'),
(186, 4, 14, '2024-05-07 07:35:52', '2024-05-07 07:35:52'),
(187, 4, 18, '2024-05-07 07:35:52', '2024-05-07 07:35:52'),
(188, 4, 20, '2024-05-07 07:35:52', '2024-05-07 07:35:52'),
(189, 7, 7, '2024-05-07 15:29:24', '2024-05-07 15:29:24'),
(190, 7, 10, '2024-05-07 15:29:24', '2024-05-07 15:29:24');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('Lzq7QWNsNYZ68NgFg88ghuz8eMlPHEo9sAasgjFZ', 10, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoicW9MczJkNXJhRWFZWWVBdkFoVFNpdmlMRzFmSFZiRnZOQjdzdmlvUiI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI5OiJodHRwOi8vMTI3LjAuMC4xOjgwMDAvcmVxdWVzdCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NDoiYXV0aCI7YToxOntzOjIxOiJwYXNzd29yZF9jb25maXJtZWRfYXQiO2k6MTcxNTA5NzA0NDt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTA7fQ==', 1715098545),
('sPNPhCnQlQDESWIvLXRqZP737ZVO07a2LkwF7GzT', 5, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiRTQwVHVYUkpDT3BlN0FMNE5TZkF6MXB2bTFpdXJUMWZMb1BqamJmTSI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI5OiJodHRwOi8vMTI3LjAuMC4xOjgwMDAvcmVxdWVzdCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NDoiYXV0aCI7YToxOntzOjIxOiJwYXNzd29yZF9jb25maXJtZWRfYXQiO2k6MTcxNTA5NjIwMDt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6NTt9', 1715097406);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `role_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `photo` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `language`, `role_id`, `status`, `photo`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Phalla', 'phallaleang731@gmail.com', NULL, '$2y$12$H/xoCxvAVtk2/EcaTnTdzupCaL7Y2CPLdCaJeYgGXfggl2NNeq5RW', 'en', 2, 1, '', NULL, '2024-05-01 09:22:31', '2024-05-05 09:27:31'),
(4, 'Team Leader', 'teamleader@gmail.com', NULL, '$2y$12$zQJf5Z1E5ojDc2H5NIDCI.bziyWKiAfIyI.GDl5LMONIYUhfZlwcu', 'en', 3, 1, '', NULL, '2024-05-02 08:21:23', '2024-05-07 09:03:42'),
(5, 'owner', 'owner@gmail.com', NULL, '$2y$12$nnGVaQ73jwo898gKNtTA9OBJz9ZOxis7Ca1uFWD/BblRrLVOHQ8SW', 'en', 1, 1, 'Live-Streaming-vs.-Live-Broadcasting.png', NULL, '2024-05-04 00:58:08', '2024-05-06 16:41:43'),
(6, 'HR Manager', 'hrmanager@gmail.com', NULL, '$2y$12$AcI/RCJ5YYkF5thSybocDuVCCE88SDDKu1WoJkVkANBg9MxVnHP..', 'en', 4, 1, NULL, NULL, '2024-05-05 09:25:42', '2024-05-06 17:09:22'),
(7, 'CEO', 'ceo@gmail.com', NULL, '$2y$12$YLEOSoUm38yiYa/Q6gT0BeR7aorgsdijGWCSh6WnWWnc4K/BzTLTW', 'en', 5, 1, NULL, NULL, '2024-05-05 09:26:15', '2024-05-07 15:42:53'),
(8, 'CFO', 'cfo@gmail.com', NULL, '$2y$12$odfgPotC5Q18iXIXAN1pN.frSwvNjWrqb.2uGVVtTbJenrESMU5MG', 'en', 6, 1, NULL, NULL, '2024-05-05 09:26:48', '2024-05-05 09:26:48'),
(9, 'System admin', 'systemadmin@gmail.com', NULL, '$2y$12$e5aPtw6Z6QS6f4w.vMquMeDwl92/gTM.ro9O.mrI124imSlDHvHNW', 'en', 7, 1, NULL, NULL, '2024-05-05 09:30:40', '2024-05-05 09:30:40'),
(10, 'Testing', 'testing@gmail.com', NULL, '$2y$12$PAEmzrnfj2lYIrSkg5mhfemkwLjsGRLaCYUGJhJp3S.YMyTjOStj.', 'en', 2, 1, NULL, NULL, '2024-05-07 02:36:06', '2024-05-07 02:36:06');

-- --------------------------------------------------------

--
-- Table structure for table `user_departments`
--

CREATE TABLE `user_departments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_departments`
--

INSERT INTO `user_departments` (`id`, `user_id`, `department_id`, `created_at`, `updated_at`) VALUES
(2, 4, 2, '2024-05-02 15:21:23', '2024-05-02 15:21:23'),
(5, 1, 2, '2024-05-02 16:31:05', '2024-05-02 16:31:05'),
(7, 5, 2, '2024-05-04 07:58:08', '2024-05-04 07:58:08'),
(9, 6, 2, '2024-05-05 16:25:42', '2024-05-05 16:25:42'),
(10, 6, 3, '2024-05-05 16:25:42', '2024-05-05 16:25:42'),
(11, 7, 2, '2024-05-05 16:26:15', '2024-05-05 16:26:15'),
(12, 7, 3, '2024-05-05 16:26:15', '2024-05-05 16:26:15'),
(13, 8, 2, '2024-05-05 16:26:48', '2024-05-05 16:26:48'),
(14, 8, 3, '2024-05-05 16:26:48', '2024-05-05 16:26:48'),
(17, 10, 3, '2024-05-07 02:36:06', '2024-05-07 02:36:06'),
(19, 9, 2, '2024-05-07 15:56:16', '2024-05-07 15:56:16'),
(20, 9, 3, '2024-05-07 15:56:16', '2024-05-07 15:56:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_workflows`
--
ALTER TABLE `department_workflows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requests_user_id_foreign` (`user_id`),
  ADD KEY `requests_department_id_foreign` (`department_id`);

--
-- Indexes for table `request_approvals`
--
ALTER TABLE `request_approvals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_departments`
--
ALTER TABLE `user_departments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `department_workflows`
--
ALTER TABLE `department_workflows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `request_approvals`
--
ALTER TABLE `request_approvals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_departments`
--
ALTER TABLE `user_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
