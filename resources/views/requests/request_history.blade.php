@extends('layouts.master')
@section('title')
{{__('setting.request')}}
@endsection
@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="nav-icon fas fa-chart-bar"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.request_history')}}</span>
              </h6>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-12">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr>
                        <th>{{__('setting.n.o')}}</th>
                        <th>{{__('setting.user_name')}}</th>
                        <th>{{__('setting.department_name')}}</th>
                        <th>{{__('setting.type')}}</th>
                        <th>{{__('setting.status')}}</th>
                        <th>{{__('setting.submit_at')}}</th>
                        <th>{{__('setting.approved_by')}}</th>
                      </tr>
                    </thead>
                    <tbody>
                      {{-- how increase $i with pagigation --}}
                      @php($i=1) 
                      @if (!empty($requestApprovals->count()>0)) 
                      @foreach($requestApprovals as $requestApproval)
                       <tr>
                        <td>{{$i++}}</td>
                        <td>{{$requestApproval->user_name}}</td> 
                        <td>{{$requestApproval->department_name}}</td>
                        <td> @if($requestApproval->request_type == 'Mission')
                          <span class="badge badge-info">{{__('setting.mission')}}</span>
                          @else
                          <span class="badge badge-warning">{{__('setting.leave')}}</span>
                          @endif
                      </td>
                        <td> @if($requestApproval->status == 'Pending')
                          <span class="badge badge-info">{{__('setting.pending')}}</span>
                          @elseif($requestApproval->status== 'Approved')
                          <span class="badge badge-success">{{__('setting.approved')}}</span>
                          @elseif($requestApproval->status == 'Rejected')
                          <span class="badge badge-danger">{{__('setting.rejected')}}</span>
                          @endif
                        </td>
                        <td>{{$requestApproval->created_at}}</td>
                        <td>{{$requestApproval->approve_by}}</td>
                        </tr>
                        @endforeach
                      @else
                      <tr>
                        <td colspan="8" class="dataTables_empty">{{__('setting.no_data_available_in_the_table')}}</td>
                    <tr>
                       @endif
                    </tbody>
                  </table>
                 </div>
                 @if($requestApprovals->total() > 10)
                 <div class="card-footer clearfix">
                    {{ $requestApprovals->appends(request()->input())->links() }}
                </div>
                @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @section('js')
  <script>      
      
</script>
@endsection
  @endsection