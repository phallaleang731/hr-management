
@extends('layouts.master')
@section('title')
{{__('setting.add_role')}}
@endsection
@section('content')

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-plus"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.add_request')}}</span>
              </h6>
            
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-8 p-10">
                
                  <form method="POST" action="{{route('request.store')}}" class="needs-validation" novalidate>
                    @csrf
                    <div class="form-group">
                      <label for="name">{{__('setting.request_type')}}</label><sup class="text-danger">*</sup>
                      <select class="custom-select" name="type" id="inputGroupSelect01" style="cursor: pointer;">
                         <option value="Leave">{{ __('setting.leave')}}</option>
                         <option value="Mission">{{ __('setting.mission')}}</option>
                        </select>
                    </div>
                    @if(!isOwner())
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                    @endif
                     @if(isOwner())
                    <div class="form-group">
                        <label for="name">{{ __('setting.user')}}</label><sup class="text-danger">*</sup>
                        <select class="custom-select user_id" name="user_id" id="inputGroupSelect01" data-placeholder="Select a role" style="cursor: pointer;" required>
                            <option value="">{{__('setting.select_user')}}</option>
                           @foreach($users as $user)
                           <option value="{{ $user->id }}">{{$user->name }}</option>
                           @endforeach
                          </select>
                    </div>
                    <span class="invalid-feedback">
                        The field is required.
                      </span>
                    <div class="form-group">
                        <label for="name">{{__('setting.user_department')}}</label><sup class="text-danger">*</sup>
                        <span id="loading_data_icon1"></span>
                        <select class="form-control select2bs4 user-department" name="department_id" id="inputGroupSelect02 " required>
                           <option selected disabled value="">{{__('setting.select_user_to_load_department')}}</option> 
                          </select>
                    </div>
                    @else
                    <div class="form-group">
                        <label for="name">{{ __('setting.user_department')}}</label><sup class="text-danger">*</sup>
                        <select class="custom-select" name="department_id" id="inputGroupSelect01" data-placeholder="Select a role" style="cursor: pointer;" required>
                           @foreach($departments as $department)
                           <option value="{{ $department->id }}">{{$department->name }}</option>
                           @endforeach
                          </select>
                    </div>
                    @endif
                  <div class="form-group">
                        <label>Start date</label><sup class="text-danger">*</sup>
                        <div class="input-group date" id="start_date" data-target-input="nearest">
                        <input type="text" name="start_date" class="form-control datetimepicker-input" data-target="#start_date" required>
                        <div class="input-group-append" data-target="#start_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>End date</label><sup class="text-danger">*</sup>
                        <div class="input-group date" id="end_date" data-target-input="nearest">
                        <input type="text" name="end_date" class="form-control datetimepicker-input" data-target="#end_date" required>
                        <div class="input-group-append" data-target="#end_date" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        </div>
                    </div>
                    <label for="decription">{{__('setting.reason')}}</label><sup class="text-danger">*</sup>
                    <textarea name="reason" id="reason" rows="10" class="form-control">{{old('reason')}}</textarea>
                    <br>
                    <input type="submit" class="btn btn-success" value="{{__('setting.save')}}">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @section('js')
    <script>
        $(function () {
        //Date range picker
        $('#start_date').datetimepicker({
            format: 'D/MM/yyyy',
        });

        $('#end_date').datetimepicker({
            format: 'D/MM/yyyy',
        });
        })

            //  getSubcategrory
        $('.user_id').on('change', function(){
          let parentId= $(this).val();
          var loading=$('#loading_data_icon2');
          var token = $("input[name= '_token']").val();
          var url = "{{url('/')}}"+'/request/user-department/'+parentId;
        if(parentId){
        $.ajax({
              type : 'GET',
              url : url,
              dataType: "json",
              beforeSend: function(request){
                loading.html('<i class="fa fa-spinner fa-pulse fa-2x fa-fw text-primary fs-2"></i>'); 
              return request.setRequestHeader('X-CSRF-Token', token);
              },
              success : function(data){
                console.log(data);
                let dataResult = data.user_department;
                  loading.html('');
                if(dataResult ==''){
                    $('.user-department').html('<option selected value="0">{{trans('setting.not_found_department')}}</option>'); 
                  }else{
                    $('.user-department').html('<option selected disabled value="">{{trans('setting.select_department')}}</option>'); 
                }
                $.each(data.user_department,function(index,subcategory){
                    var tr_html = '<option selected value="0">{{trans('class.no_data')}}</option>';
                    var tr_html = '<option value="'+subcategory.pivot.department_id+'">'+subcategory.name+'</option>';
                  
                $('.user-department').append(tr_html);
              })
              }

            })
      }else{
        $('.user-department').html('<option selected value="0">{{trans('setting.not_found_department')}}</option>');
      }
         })
    </script>
    @endsection
  @endsection