@extends('layouts.master')
@section('title')
{{__('setting.request')}}
@endsection
@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="nav-icon fas fa-chart-bar"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.request')}}</span>
              </h6>
              <h6 class="float-right">
                <span data-toggle="tooltip" role="button" type="button" aria-haspopup="true" title="{{__('setting.add_request')}}"> 
                  @if(authorize('can create request'))
                    <a href="{{route('request.create')}}" type="button">
                      <button class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{__('setting.add_request')}}</button>
                    </a>
                @endif
                </span>
              </h6>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-12">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr>
                        <th class="col-2 " style="white-space: nowrap;">{{__('setting.n.o')}}</th>
                        <th  class="col-lg-2" style="white-space: nowrap;">{{__('setting.user_name')}}</th>
                        <th style="white-space: nowrap;">{{__('setting.department_name')}}</th>
                        <th style="white-space: nowrap;">{{__('setting.type')}}</th>
                        <th style="white-space: nowrap;">{{__('setting.status')}}</th>
                        <th class="col-lg-2" style="white-space: nowrap;">{{__('setting.reason')}}</th>
                        <th style="white-space: nowrap;">{{__('setting.start_date')}}</th>
                        <th style="white-space: nowrap;">{{__('setting.end_date')}}</th>
                        <th style="white-space: nowrap">{{__('setting.total_request_date')}}</th>
                        <th style="white-space: nowrap;">{{__('setting.submit_at')}}</th>
                        @if(authorize('can approve request') || authorize('can view any request'))
                        <th class="text-center" style="white-space: nowrap;">{{__('setting.action')}}</th>
                        @endif
                      </tr>
                    </thead>
                    <tbody>
                      @php($i=1) 
                      @if (!empty($requests->count()>0)) 
                      @foreach($requests as $request)
                       <tr>
                        <td>{{$i++}}</td>
                        <td>{{$request->user_name}}</td> 
                        <td class="col-lg-2">{{$request->department_name}}</td>
                        <td> @if($request->type == 'Mission')
                          <span class="badge badge-info">{{__('setting.mission')}}</span>
                          @else
                          <span class="badge badge-warning">{{__('setting.leave')}}</span>
                          @endif
                      </td>
                        <td> @if($request->status == 'Pending')
                          <span class="badge badge-info">{{__('setting.pending')}}</span>
                          @elseif($request->status== 'Approved')
                          <span class="badge badge-success">{{__('setting.approved')}}</span>
                          @elseif($request->status == 'Rejected')
                          <span class="badge badge-danger">{{__('setting.rejected')}}</span>
                          @endif
                        </td>
                        <td class="col-lg-2" style="white-space: nowrap">{!! htmlspecialchars_decode(limitDescription($request->reason)) !!}</td>
                        <td >{{$request->start_date}}</td>
                        <td>{{$request->end_date}}</td>
                        <td class="text-center">{{$request->total_days}}</td>
                        <td style="white-space: nowrap">{{$request->created_at}}</td>
                        @if(authorize('can approve request') || authorize('can view any request'))
                        <td class="d-flex justify-content-center align-items-center">
                          @if(authorize('can approve request'))
                            @if($request->status == 'Pending')
                            <a href="#" type="button" data-toggle="tooltip" class="btn bpo text-primary"  data-content="<div style='width:150px;'><p>{{__('setting.are_you_sure')}}</p><a class='btn btn-success' href='{{route('request.approve.store',['id' =>  $request->id, 'status' => 'Approved'])}}'>{{__('setting.approve')}}</a> <a class='btn btn-danger' href='{{route('request.approve.store', ['id' => $request->id, 'status' => 'Rejected'])}}'>{{__('setting.reject')}}</a></div>" data-html="true" data-placement="left"> 
                                <button class="btn btn-sm btn-default">{{__('setting.approve')}}
                                </button>
                            </a>
                            @endif
                          @endif
                          @if(authorize('can view any request'))
                          <a href="{{route('request.request.history',$request->id)}}" style="color:rgb(20, 18, 18);" type="button" data-toggle="tooltip" data-placement="top" title="{{__('setting.view_history')}}">
                            <button class="btn btn-sm btn-default">{{__('setting.view')}} </button>
                          </a>
                          @endif
                      </td>
                      @endif
                        </tr>
                        @endforeach
                      @else
                      <tr>
                        <td colspan="8" class="dataTables_empty">{{__('setting.no_data_available_in_the_table')}}</td>
                    <tr>
                       @endif
                    </tbody>
                  </table>
                 </div>
                 @if($requests->total() > 10)
                 <div class="card-footer clearfix">
                    {{ $requests->appends(request()->input())->links() }}
                </div>
                @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @section('js')
  <script>      
      
</script>
@endsection
  @endsection