@extends('layouts.master')
@section('title')
{{__('setting.dashboard')}}
@endsection
@section('content')
  <!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-success card-outline">
          <div class="card-header card-profile">
            <h6 class="card-title text-success text-bold">
                 &nbsp;  &nbsp;<span>{{ __('user.my_profile')}}</span>
              </h6>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-12 col-lg-12">
                <div style="margin-top:20px;" class="row">
                  <div class="container emp-profile">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="profile-img">
                            <img width="50%" onclick="showImage(this)" src="{{Auth::user()->photo_url}}" class="img-responsive img-thumbnail" style="cursor: pointer;" alt=""/>
                         </div>
                       </div>
                        <div class="col-md-6">
                          <div class="profile-head">
                            <h5><b>  {{Auth::user()->name }}</b></h5><br>
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">{{__('user.about')}}</a>
                                    </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                           <div class="tab-content" id="custom-tabs-one-tabContent">
                                    <!-- about -->
                              <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                <div class="row">
                                  <div class="col-sm-0 col-lg-4">
                                  </div>
                                  <div class="col-sm-12 col-lg-8">
                                    <div class="row">
                                        <div class="col-3">
                                           <b class="text-success">{{__('user.name')}} :</b>
                                        </div>
                                        <div class="col-9">
                                              {{Auth::user()->name}}
                                        </div>
                                         </div><br>
                                      <div class="row">
                                        <div class="col-3"><b class="text-success">{{__('user.email')}} :</b> </div>
                                            <div class="col-9">{{maskText(Auth::user()->email)}} </div>
                                              </div><br>
                                        <div class="row">
                                                <div class="col-3"><b class="text-success">{{__('setting.department')}} :</b></div>
                                                <div class="col-9">
                                                  @foreach(Auth::user()->department_names as $department_name)
                                                    <span class="badge badge-info">{{$department_name}}</span>
                                                  @endforeach
                                                </div>
                                            </div><br>
                                      @if(isOwner())
                                      <div class="row">
                                          <div class="col-3"><b class="text-success">{{__('user.role')}} :</b></div>
                                          <div class="col-9"> {{Auth::user()->role_name}} </div>
                                      </div><br>
                                      @endif
                                      <div class="row">
                                        <div class="col-3"><b class="text-success">{{__('user.password')}} :</b></div>
                                         <div class="col-9">*******</div>
                                      </div>
                                      </div>
                                    </div>
                                    </div>
                  
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
           </section>
        @section('js')
      <script>
        function preview(e){

        var img_link = document.getElementById('prev');
             img_link.src = URL.createObjectURL(e.target.files[0]);
         
        }
      </script>
    @endsection
@endsection