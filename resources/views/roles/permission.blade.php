@extends('layouts.master')
@section('title')
{{__('setting.permission')}}
@endsection
@section('content')
  <!-- Main content -->
  @csrf
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-key"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.set_permissions')}} <span class="text-dark">( {{$role->name}} )</span></span>
              </h6>
            </div>
            <form method="POST" action="{{route('role.permission.store')}}" class="needs-validation" novalidate>
              @csrf
                <input type="hidden" name="role_id" value="{{$role->id}}">
            <div class="card-body">
              <div class="row">
                @foreach($permissions as $permission)
                <div class="col-12 col-lg-12">
                  <ul class="list-group mb-4">
                    <li class="list-group-item bg-light" aria-current="true">{{$permission->name}}</li>
                    <li class="list-group-item">
                        <div class="row">
                            @if($permission->children->count() > 0)
                                @foreach($permission->children as $child)
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                        <div class="p-2 border mt-1 mb-2">
                                            <div class="icheck-success d-inline">
                                                <input type="checkbox" name="permissions[]" id="permission{{$child->id}}"  {{ in_array($child->id, $rolePermissions) ? 'checked' : '' }} value="{{$child->id}}">
                                                <label class="text-sm"  for="permission{{$child->id}}"> <span style="font-size: 12px; color:#343a40;">{{__('permission.'.$child->name)}}</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </li>
                    </ul>
                </div>
                @endforeach
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-success">{{__('setting.save')}}</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  @section('js')
  <script>      
</script>
@endsection
@endsection