@extends('layouts.master')
@section('title')
{{__('setting.roles')}}
@endsection
@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-key"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.user_groups')}}</span>
              </h6>
              <h6 class="float-right">
                <span data-toggle="tooltip" role="button" type="button" aria-haspopup="true" title="{{__('setting.add_role')}}">
                    @if(authorize('can create role')) 
                        <a href="{{route('role.create')}}" type="button">
                        <button class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{__('setting.add_role')}}</button>
                        </a>
                    @endif
                </span>
              </h6>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-12">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr>
                        <th>{{__('setting.n.o')}}</th>
                        <th>{{__('setting.name')}}</th>
                        <th>{{__('setting.description')}}</th>
                        @if(authorize('can edit role') || authorize('can change permission') || authorize('can delete role')) 
                        <th class="text-center">{{__('setting.action')}}</th>
                        @endif
                      </tr>
                    </thead>
                    <tbody>
                      @php($i=1)
                      @foreach($roles as $role)
                       @if ($roles) 
                       <tr>
                        <td>{{$i++}}</td> 
                        <td>{{$role->name}}</td>
                        <td>{!! htmlspecialchars_decode($role->description) !!}</td>
                        @if(authorize('can edit role') || authorize('can change permission') || authorize('can delete role')) 
                        <td>
                        @if(authorize('can change permission'))
                            <a href="{{route('role.permission', $role->id)}}" class="text-success" type="button" data-toggle="tooltip" data-placement="top" title="update role"><i class="fas fa-bars"></i>
                            </a> 
                            &nbsp; 
                            &nbsp;
                        @endif
                            @if($role->name != 'User' && $role->name != 'Owner')
                                @if(authorize('can edit role'))
                                <a href="{{route('role.edit', $role->id)}}" class="text-success" type="button" data-toggle="tooltip" data-placement="top" title=""><i class="fa fa-edit"></i>
                                </a>
                                @endif
                                @if(authorize('can delete role'))
                                    <a href="#" type="button" data-toggle="tooltip" class="btn bpo text-danger"  data-content="<div style='width:150px;'><p>{{__('setting.are_you_sure')}}</p><a class='btn btn-danger' href='{{route('role.delete', $role->id)}}'>{{__('setting.yes_im_sure')}}</a> <span class='btn btn-primary bpo-close'>{{__('setting.no')}}</span></div>" data-html="true" data-placement="left"> <span class="badge btn-sm"><i class="fa fa-trash text-md"></i> </span>
                                </a>
                                @endif
                            @endif
                      </td>
                        @endif
                        </tr>
                      @else
                      <tr>
                        <td colspan="4" class="dataTables_empty">{{__('setting.no_data_available_in_the_table')}}</td>
                    <tr>
                       @endif
                      @endforeach
                    </tbody>
                  </table>
                  @if($roles->total() > 10)
                  <div class="card-footer clearfix">
                     {{ $roles->appends(request()->input())->links() }}
                 </div>
                 @endif
                 </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @section('js')
  <script>      
      
</script>
@endsection
  @endsection