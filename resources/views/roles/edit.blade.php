
@extends('layouts.master')
@section('title')
{{__('setting.edit_role')}}
@endsection
@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-edit"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.edit_role')}}</span>
              </h6>
            
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-8 p-10">
                  <form method="post" action="{{route('role.update',$role->id)}}" class="needs-validation" novalidate>
                    @csrf
                    @method('PATCH')
                    <label for="name">{{__('setting.name')}}</label><sup class="text-danger">*</sup>
                    <input type="text" name="name" value="{{$role->name}}" class="form-control" id="name" required>
                    <label for="decription">{{__('setting.description')}}</label>
                    <textarea name="description" id="description" rows="10" class="form-control">
                        {{$role->description}}
                    </textarea>
                    <br>
                    <input type="submit" class="btn btn-success" value="{{__('setting.update')}}">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endsection