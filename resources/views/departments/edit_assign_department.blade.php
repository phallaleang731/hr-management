
@extends('layouts.master')
@section('title')
{{__('setting.edit_department')}}
@endsection
@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-edit"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.department_work_flow')}}</span>
              </h6>
            
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-8 p-10">
                  <form method="POST" action="{{route('department.assign.update', $departmentWorkFlows->id)}}" class="needs-validation" novalidate>
                    @csrf
                    @method('PATCH')
                    <label for="name">{{__('setting.department_name')}}</label>
                    <input type="text"  disabled value="{{$departmentWorkFlows->department_name}}" class="form-control" id="name" required>
                    <input type="hidden" name="department_id" value="{{$departmentWorkFlows->department_id}}">
                    <br>
                    <div class="form-group">
                      <label for="name">{{__('setting.request_type')}}</label>
                      <select disabled class="custom-select" name="type" id="inputGroupSelect01" style="cursor: pointer;">
                         <option value="Leave" {{$departmentWorkFlows->type=='Leave' ? 'selected' : ''}}>{{ __('setting.leave')}}</option>
                         <option value="Mission" {{$departmentWorkFlows->type== 'Mission' ? 'selected' : ''}}>{{ __('setting.mission')}}</option>
                        </select>
                  </div>
                  <div id="step_role" class="row">
                    <?php 
                    $roleName = '';
                    $roleId = '';
                    if (!empty($departmentWorkFlows->step)) {
                        $departmentWorkFlows->step = json_decode($departmentWorkFlows->step);
                        foreach ($departmentWorkFlows->step as $step) { 
                            $roleName = App\Models\Role::find($step->role_id)->name;
                    ?>
                    <div class="mr-2 d-flex align-items-center badge badge-light">
                        <?php echo $roleName ?>
                        <input type="hidden" name="step[]" value="<?php echo $step->role_id ?>">
                        <button type="button" class="pb-1 ml-1 close"><span aria-hidden="true" class="text-danger">×</span></button>
                    </div>
                    <?php 
                        } 
                    } 
                    ?>
                  </div>
                  <br>
                    <div class="form-group">
                      <label>Role</label>
                      <select class="custom-select role_id" name="" id="inputGroupSelect01" data-placeholder="Select a role" style="cursor: pointer;">
                        <option value="">{{__('setting.select_role')}}</option>
                        @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->name }}</option>
                        @endforeach
                    </select>
                  </div>
                    <input type="submit" class="btn btn-success" value="{{__('setting.save')}}">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  @section('js')
  <script>
   $(document).ready(function() {
    // Add step when role selection changes
    $('.role_id').change(function() {
    var role_id = $(this).val();
    // check if the role is already added
    var exists = false;
    $('#step_role input[type="hidden"]').each(function() {
        if ($(this).val() == role_id) {
            exists = true;
            return false; // Exit the loop early since the role is already added
        }
    });
    if (!exists) {
        var role_name = $(this).find('option:selected').text();
        // Create the step HTML
        var step = '<div class="mr-2 d-flex align-items-center badge badge-light">';
        step += role_name;
        step += '<input type="hidden" name="step[]" value="' + role_id + '">';
        step += '<button type="button" class="pb-1 ml-1 close"><span aria-hidden="true" class="text-danger">×</span></button>';
        step += '</div>';
        // Append the step to the container
        $('#step_role').append(step);
    }
});


    // Remove step when close button is clicked
    $(document).on('click', '.close', function() {
        $(this).parent().remove();
    });
});
</script>
@endsection
@endsection