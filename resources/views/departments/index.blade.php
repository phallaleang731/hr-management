@extends('layouts.master')
@section('title')
{{__('setting.roles')}}
@endsection
@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-user"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.department')}}</span>
              </h6>
              <h6 class="float-right">
                <span data-toggle="tooltip" role="button" type="button" aria-haspopup="true" title="{{__('setting.add_department')}}">
                  @if(authorize('can create department')) 
                    <a href="{{route('department.create')}}" type="button">
                      <button class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{__('setting.add_department')}}</button>
                    </a>
                  @endif
                </span>
              </h6>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-12">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr>
                        <th>{{__('setting.n.o')}}</th>
                        <th>{{__('setting.name')}}</th>
                        @if(authorize('can assign department') || authorize('can edit department') || authorize('can delete department'))
                        <th class="text-center">{{__('setting.action')}}</th>
                        @endif
                      </tr>
                    </thead>
                    <tbody>
                      @if (!empty($departments))
                      @php($i=1)
                      @foreach($departments as $department)
                      
                       <tr>
                        <td>{{$i++}}</td> 
                        <td>{{$department->name}}</td>
                        @if(authorize('can assign department') || authorize('can edit department') || authorize('can delete department'))
                        <td class="text-center">
                          @if(authorize('can assign department'))
                            <a href="{{route('department.assign',$department->id)}}" class="text-success" type="button" data-toggle="tooltip" data-placement="top" title="{{__('setting.assign_department')}} {{$department->name}}"><i class="fas fa-bars"></i>
                            </a>
                              &nbsp; 
                              &nbsp; 
                          @endif
                          @if(authorize('can edit department'))
                          <a href="{{route('department.edit', $department->id)}}" class="text-success" type="button" data-toggle="tooltip" data-placement="top" title="{{__('setting.edit_department')}} {{$department->name}}"><i class="fa fa-edit"></i>
                          </a>
                          @endif
                          @if(authorize('can delete department'))
                          <a href="#" type="button" data-toggle="tooltip" class="btn bpo text-danger"  data-content="<div style='width:150px;'><p>{{__('setting.are_you_sure')}}</p><a class='btn btn-danger' href='{{route('department.delete', $department->id)}}'>{{__('setting.yes_im_sure')}}</a> <span class='btn btn-primary bpo-close'>{{__('setting.no')}}</span></div>" data-html="true" data-placement="left"> <span class="badge btn-sm"><i class="fa fa-trash text-md"></i> </span>
                        </a>
                         @endif
                      </td>
                      @endif
                        </tr>
                      @endforeach
                      @else
                      <tr>
                       <td colspan="3" class="text-center">{{__('setting.no_data_available_in_the_table')}}</td>
                    <tr>
                       @endif
                    </tbody>
                  </table>
                 </div>
                 @if($departments->total() > 10)
                 <div class="card-footer clearfix">
                    {{ $departments->appends(request()->input())->links() }}
                </div>
                @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
  </section>
  @endsection