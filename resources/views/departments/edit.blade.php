
@extends('layouts.master')
@section('title')
{{__('setting.edit_department')}}
@endsection
@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h6 class="card-title text-primary text-bold">
                <i class="fas fa-edit"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.edit_department')}}</span>
              </h6>
            
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-8 p-10">
                
                  <form method="POST" action="{{route('department.update', $department->id)}}" class="needs-validation" novalidate>
                    @csrf
                    @method('PATCH')
                    <label for="name">{{__('setting.name')}}<sup class="text-danger">*</sup></label>
                    <input type="text" name="name" value="{{$department->name}}" class="form-control" id="name" required>
                    <br>
                    <input type="submit" class="btn btn-primary" value="{{__('setting.update')}}">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endsection