
@extends('layouts.master')
@section('title')
{{__('setting.edit_department')}}
@endsection
@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          @if($departmentWorkFlows->pluck('type')->count() < 2)
          @if(authorize('can create workflow department'))
          {{-- count departmentWorkFlows->type --}}
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-edit"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.department_work_flow')}}</span>
              </h6>
            
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-8 p-10">
                  <form method="POST" action="{{route('department.assign.store')}}" class="needs-validation" novalidate>
                    @csrf
                    <label for="name">{{__('setting.department_name')}}</label>
                    <input type="text"  disabled value="{{$department->name}}" class="form-control" id="name" required>
                    <input type="hidden" name="department_id" value="{{$department->id}}">
                    <br>
                    <div class="form-group">
                      <label for="name">{{__('setting.request_type')}}</label>
                      <select class="custom-select" name="type" id="inputGroupSelect01" style="cursor: pointer;">
                         <option value="Leave">{{ __('setting.leave')}}</option>
                         <option value="Mission">{{ __('setting.mission')}}</option>
                        </select>
                  </div>
                  <div id="step_role" class="row">
                    <div class="d-flex">
                    </div>
                  </div>
                  <br>
                    <div class="form-group">
                      <label>Role</label>
                      {{-- <select class="select2 mul-select" multiple="multiple" name="step[]" data-placeholder="Select a department" style="width: 100%;"> --}}
                        <select class="custom-select role_id" name="" id="inputGroupSelect01" data-placeholder="Select a role" style="cursor: pointer;">
                          <option value="">{{__('setting.select_role')}}</option>
                          @foreach($roles as $role)
                          <option value="{{$role->id}}">{{$role->name }}</option>
                          @endforeach
                      </select>
                  </div>
                    <input type="submit" class="btn btn-success" value="{{__('setting.save')}}">
                  </form>
                </div>
              
              </div>
            </div>
          </div>
          @endif
          @endif
        </div>
      </div>
      
      <div class="col-12 col-lg-12">
        <div class="table-responsive">
          <table class="table table-bordered table-striped dtr-inline" role="grid" aria-describedby="example1_info">
            <thead>
              <tr>
                <th>{{__('setting.n.o')}}</th>
                <th>{{__('setting.department_name')}}</th>
                <th>{{__('setting.request_type')}}</th>
                <th>{{__('setting.step')}}</th>
                @if(authorize('can edit workflow department'))
                <th class="text-center">{{__('setting.action')}}</th>
                @endif
              </tr>
            </thead>
            <tbody>
              @if (!empty($departmentWorkFlows))
              @php($i=1)
              @foreach($departmentWorkFlows as $departmentWorkflow)
               <tr>
                <td>{{$i++}}</td> 
                <td>{{$departmentWorkflow->department_name}}</td>
                <td> @if($departmentWorkflow->type == 'Mission')
                  <span class="badge badge-info">{{__('setting.mission')}}</span>
                  @else
                  <span class="badge badge-warning">{{__('setting.leave')}}</span>
                  @endif
              </td>
                <td>
                  @if(!empty($departmentWorkflow->step))
                  @foreach(json_decode($departmentWorkflow->step) as $step)
                  {{-- list --}}
                  <?php $role= app('App\Models\Role')->where('id',$step->role_id)->first() ?>
                 <span>{{$step->order_number}}</span> - <span>{{$role->name ?? ''}}</span> <br>

                  @endforeach
                  @endif
                </td>
                @if(authorize('can edit workflow department'))
                  <td class="text-center">
                    <a href="{{route('department.assign.edit', $departmentWorkflow->id)}}" class="text-success" type="button" data-toggle="tooltip" data-placement="top" title="{{__('setting.edit_department')}} {{$department->name}}"><i class="fa fa-edit"></i>
                    </a>
                </td>
                @endif
                </tr>
              @endforeach
              @else
              <tr>
               <td colspan="5" class="text-center">{{__('setting.no_data_available_in_the_table')}}</td>
            <tr>
               @endif
            </tbody>
          </table>
         </div>
        </div>
    </div>
  </section>
  {{-- js --}}
  @section('js')
  <script>
    $(document).ready(function() {
      $('.role_id').change(function() {
        var role_id = $(this).val();
        var role_name = $(this).find('option:selected').text();
        // Create the step HTML
        var step = '<div class="mr-2 d-flex align-items-center badge badge-light">';
        step += role_name;
        step += '<input type="hidden" name="step[]" value="' + role_id + '">';
        step += '<button type="button" class="pb-1 ml-1 close"><span aria-hidden="true" class="text-danger">×</span></button>';
        step += '</div>';
        
        // Append the step to the container
        $('#step_role').append(step);
      });

    // Remove step when close button is clicked
      $(document).on('click', '.close', function() {
          $(this).parent().remove();
      });
    });
  </script>
  @endsection
  @endsection