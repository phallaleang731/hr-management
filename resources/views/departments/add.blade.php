
@extends('layouts.master')
@section('title')
{{__('setting.add_department')}}
@endsection
@section('content')

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-plus"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.add_department')}}</span>
              </h6>
            
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-8 p-10">
                
                  <form method="POST" action="{{route('department.store')}}" class="needs-validation" novalidate>
                    @csrf
                    <label for="name">{{__('setting.name')}}<sup class="text-danger">*</sup></label>
                    <input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" required>
                    <span class="invalid-feedback">
                        The field is required.
                      </span>
                    <br>
                    <input type="submit" class="btn btn-success" value="{{__('setting.save')}}">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endsection