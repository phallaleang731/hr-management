<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel = "icon" type = "image/png" href = "">
  <title>@yield('title', config('app.name'))| HR Systme</title>
  @php ($currentURL = url()->current())
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/daterangepicker/daterangepicker.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote-bs4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/datepicker.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/toggle.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/video.css')}}">
  <link rel="stylesheet" href="{{asset('assets/js/multiple.js')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
  <link href='https://fonts.googleapis.com/css?family=Khmer' rel='stylesheet'>
    <link rel="stylesheet" href="{{asset('assets/css/jquery-ui.css')}}">
  @yield('css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  {{-- <div class="preloader flex-column justify-content-center align-items-center"> --}}
   <!--  <img  src="{{asset('img/ajax-loader.gif')}}"> -->
   {{-- <span class="loader"></span> --}}
  {{-- </div> --}}

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-light text-md"  >
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars text-black"></i></a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a href="#" class="nav-link" id="navbarProfile" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
          <img src="{{asset(Auth::user()->language=='km' ? 'img/khmer.png' : 'img/english.png' )}}" alt="" width="20" height="20"  class="brand-img"> 
          &nbsp;<span class="text-black">{{Auth::user()->language=='km' ? __('header.khmer') : __('header.english')}}</span> <span> &nbsp;<i class="mr-2 fa fa-caret-down text-black"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarProfile">
          <a class="dropdown-item {{Auth::user()->language=='km' ? 'active' : ''}}" href="{{route('changeLang', 'km') }}">
            <img src="{{asset('img/khmer.png')}}" width="18">&nbsp; {{__('header.khmer')}}
          </a>
          <a class="dropdown-item {{Auth::user()->language=='en' ? 'active' : ''}}" href="{{route('changeLang', 'en')}}">
            <img src="{{asset('img/english.png')}}" width="18">&nbsp; {{__('header.english')}}
          </a>
        </div>
       
      </li>
        <li class="nav-item dropdown">
          {{-- back home --}}
          <a href="#" class="nav-link" id="navbarProfile" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
            <img src="{{Auth::user()->photo_url}}" alt="" width="30" height="30"  class="brand-img img-circle elevation"> 
            &nbsp;<span class="text-light">{{Auth::user()->username}}</span> <span> &nbsp;<i class="mr-2 fa fa-caret-down text-black"></i>
          </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarProfile">
              <a class="dropdown-item" href="{{route('home')}}">
                <i class="fa fa-user text-primary"></i>&nbsp; Profile
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{route('logout') }}">

                <i class="fa fa-sign-out-alt text-danger"></i>&nbsp; {{__('user.logout')}}
              </a>
            </div>
         
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
            <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
      </ul>
  </nav>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color:#001529;  {{Auth::user()->language=='km' ? 'font-family:khmer;font-size:10px' : 'font-size:12px'}}">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <span class="brand-text" style="font-size: 30px; font-weight:bold ; font-style:normal">Human Resource</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" style="{{Auth::user()->language=='km' ? 'font-family:khmer;font-size:13px' : 'font-size:15px'}}">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="{{URL('/')}}" class="nav-link {{request()->is('/') ? ' active ' : '' }}">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>{{__('header.dashboard')}}</p>
                </a>
              </li>
              @if(authorize('can view user'))
              <li class="nav-item">
                <a  href="{{route('user.index')}}" class="nav-link {{request()->is('user')  
                    || route('user.create') ==  $currentURL 
                    || request()->is('user/edit/*')  
                    == $currentURL    ? 'active' : ''}}">
                  <i class="nav-icon fas fa-user text-md"></i>
                  <p>
                    {{__('header.users')}}
                  </p>
                </a>
              </li>
              @endif
              @if(authorize('can view request'))
              <li class="nav-item">
                <a href="{{route('request.index')}}" class="nav-link {{request()->is('request')  
                    || request()->is('request/request/request-history/*')  
                    == $currentURL    ? 'active' : ''}}"> <!--active-->
                  <i class="nav-icon fas fa-chart-bar text-md"></i>
                  <p>
                    {{__('header.request')}}
                  </p>
                </a>
              </li>
              @endif
              @if(authorize('can create request'))
              <li class="nav-item">
                <a href="{{route('request.create')}}" class="nav-link {{ route('request.create')
                  == $currentURL    ? 'active' : ''}}"> <!--active-->
                  <i class="nav-icon fas fa-plus text-md"></i>
                  <p>
                    {{__('header.add_request')}}
                  </p>
                </a>
              </li>
              @endif
              @if(authorize('can view department'))
              <li class="nav-item">
                <a href="{{route('department.index')}}" class="nav-link {{request()->is('department')  
                    || route('department.create') ==  $currentURL 
                    || request()->is('department/edit/*')  == $currentURL    ? 'active' : ''}}"> <!--active-->
                      <i class="fas fa-list nav-icon text-md"></i>
                    <p>{{__('header.department')}}</p>
                  </a>
              </li>
              @endif
              @if(authorize('can view role'))
              <li class="nav-item">
                <a href="{{route('role.index')}}" class="nav-link {{request()->is('role')  
                  || route('role.create') ==  $currentURL 
                  || request()->is('role/permission/*')  == $currentURL    ? 'active' : ''}}"> <!--active-->
                  <i class="fas fa-key nav-icon text-md"></i>
                  <p>{{ __('header.group_permissions')}}</p>
                </a>
              </li>
              @endif
        </ul>
      </nav>
    </div>
  </aside>
  <div class="modal fade" id="view-img">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <div class="show-img"></div>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <div class="content-wrapper" style=" {{Auth::user()->language=='km' ? 'font-size:15px' : 'font-size:16px'}}">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 main-top-bar">
          <div class="col-sm-12">
            <ol class="breadcrumb">
               @foreach ($bc as $b) 
              @if ($b['link'] === '#') 
                  <?php echo '<li class="breadcrumb-item">' . $b['page'] . '</li>'; ?>
              @else
                 <?php echo '<li class="breadcrumb-item "><a class="text-success" href="' . $b['link'] . '">' . $b['page'] . '</a></li>' ?>  
             @endif
          @endforeach 
            </ol>
          </div>
        </div>
        @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <div>
              {{session('success')}}
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          @if(Session::has('error'))
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
          <div>
            {{session('error')}}
          </div>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
          @endif
            @if(Session::has('warning'))
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
          <div>
            {{session('warning')}}
          </div>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
          @endif
          @if ($errors->hasAny(['image', 'image.*']))
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <div>
              {{$errors->first('image') ?: $errors->first('image.*') }}
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>
        @endif
      </div>
    </div>
    @if(count($errors)>0 && $errors->any())
   @foreach($errors->all() as $error)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <div>
      {{$error}}
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endforeach
    @endif
      <div id="alert">
        
      </div> 
    @yield('content')

  </div>
  <footer class="main-footer">
    {{-- <strong>{{setting()->footer_name}} </strong> --}}
  
    <div class="float-right d-none d-sm-inline-block">
      {{-- <b>Version</b> {{setting()->version}} --}}
    </div>
  </footer>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
<!-- Setting -->
<input type="hidden" id="setting" value="" name="">
<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('assets/js/core.js')}}"></script>
<script src="{{asset('assets/js/video.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{ asset('assets/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('assets/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
{{-- <script src="{{asset('assets/plugins/jqvmap/jquery.vmap.min.js')}}"></script> --}}
{{-- <script src="{{asset('assets/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script> --}}
<!-- jQuery Knob Chart -->
<script src="{{ asset('assets/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('assets/dist/js/demo.js')}}"></script>
<script src="{{asset('assets/js/datepicker.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="{{ asset('assets/dist/js/pages/dashboard.js')}}"></script> --}}
{{-- Datable--}}
{{-- <script src="//cdn.jsdelivr.net/npm/jquery-form@4.3.0/dist/jquery.form.min.js"></script> --}}
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> -->
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>

<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="{{asset('assets/plugins/select2/js/select2.full.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>


@yield('js')

</body>
</html>
