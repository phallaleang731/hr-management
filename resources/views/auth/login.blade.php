<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> {{config('app.name')}} | Log in</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('assets/dist/css/adminlte.min.css')}}">
  <style>
    .login-page, .register-page {
    -webkit-align-items: center;
    -ms-flex-align: center;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
    height: 100vh;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
}
/*.contact-section h1 {
    color: #fff;
    font-size: 35px;
    margin-bottom: 20px;
}*/
.contact-section .typing {
   font-weight: 600;
}

.contact-section .typing > *{
    overflow: hidden;
    white-space: nowrap;
    animation: typingAnim 10s steps(50);
}

@keyframes typingAnim {
    from {width:0}
    to {width:100%}
}
  </style>
</head>
<body class="hold-transition login-page" onload="randomColor()">
<div style="padding:4px;background-color: rgb(0 0 0 / 10%);">
<div class="login-box" >
  <div class="login-logo" >
    <a href="#" class="text-white"><b>Human Resource</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body bg-white contact-section">
      <p class="login-box-msg text-info typing">LOGIN TO CONTINUE</p>
      @if(count($errors)>0)
      @foreach($errors->all() as $error)
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
      <div>
        {{$error}}
      </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
        @endforeach
      @endif

      <form action="{{route('login')}}" method="post" class="submit">
        @csrf
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="username" name="email" placeholder="Email" autocomplete>
          <div class="input-group-append">
            <div class="input-group-text bg-success">
              <span class="fas fa-user text-white"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" id="password" class="form-control" name="password" placeholder="Password" autocomplete >
          <div class="input-group-append">
            <div class="input-group-text bg-success">
              <span class="fas fa-lock text-white"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-success btn-block" >Log In</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<script src="{{ asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
</body>
<script>
  function randomColor() {
    var images = ['img/background1.jpg', 'img/background2.jpg', 'img/background3.jpg', 'img/background4.jpg', 'img/background5.jpg', 'img/background6.jpg','img/background7.png'];
            var image = images[Math.floor(Math.random() * images.length)];
            document.getElementsByClassName('login-page')[0].style.backgroundImage = "url('" + image + "')";
}


 

</script>

</html>
