@extends('layouts.master')
@section('title')
{{__('setting.users')}}
@endsection
@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-user"></i>
                 &nbsp;  &nbsp;<span>{{__('setting.users')}}</span>
              </h6>
              <h6 class="float-right">
                <span data-toggle="tooltip" role="button" type="button" aria-haspopup="true" title="{{__('setting.add_user')}}">
                  @if(authorize('can create user')) 
                    <a href="{{route('user.create')}}" type="button">
                      <button class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{__('setting.add_user')}}</button>
                    </a>
                @endif
                </span>
              </h6>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-lg-12">
                <div class="table-responsive">
                  <table class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr>
                        <th>{{__('setting.n.o')}}</th>
                        <th>{{__('setting.photo')}}</th>
                        <th>{{__('setting.name')}}</th>
                        <th>{{__('setting.email')}}</th>
                        <th>{{__('setting.role_name')}}</th>
                        <th>{{__('setting.department_name')}}</th>
                        <th>{{__('setting.status')}}</th>
                        @if(authorize('can edit user'))
                        <th class="text-center">{{__('setting.action')}}</th>
                        @endif
                      </tr>
                    </thead>
                    <tbody>
                      @php($i=1) 
                      @if ($users) 
                      @foreach($users as $user)
                       <tr>
                        <td>{{$i++}}</td>
                        <td><div class='text-center'><img src={{$user->photo_url }} onclick='showImage(this)'  width='70' class='' style='cursor:pointer' /></div></td> 
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role_name}}</td>
                        <td>
                            @if($user->userDepartments->count() > 0)
                            @foreach($user->userDepartments as $department)
                            <ul>
                                <li><span class="badge badge-info">{{$department->name}}</span></li>
                            </ul>
                            {{-- <span class="badge badge-info">{{$department->name}}</span> --}}
                            @endforeach
                            @else
                            <span class="badge badge-danger">{{__('setting.not_assigned')}}</span>
                            @endif
                        </td>
                        <td> @if($user->status == 1)
                            <span class="badge badge-success">{{__('setting.active')}}</span>
                            @else
                            <span class="badge badge-danger">{{__('setting.inactive')}}</span>
                            @endif
                        </td>
                        @if(authorize('can edit user'))
                        <td class="text-center">
                          <a href="{{route('user.edit', $user->id)}}"  style="color:rgb(20, 18, 18)" type="button" data-toggle="tooltip" data-placement="top" title="{{__('setting.edit_user')}}"><button class="btn btn-sm btn-default">{{__('setting.edit')}}
                          </a>
                        </td>
                      @endif
                        </tr>
                        @endforeach
                      @else
                      <tr>
                        <td colspan="4" class="dataTables_empty">{{__('setting.no_data_available_in_the_table')}}</td>
                    <tr>
                       @endif
                    </tbody>
                  </table>
                 </div>
                 @if($users->total() > 10)
                 <div class="card-footer clearfix">
                    {{ $users->appends(request()->input())->links() }}
                </div>
                @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @section('js')
  <script>      
      
</script>
@endsection
  @endsection