
@extends('layouts.master')
@section('title')
{{__('user.add_users')}}
@endsection
@section('content')
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-user-plus"></i>
                 &nbsp;  &nbsp;<span>{{__('user.add_users')}}</span>
              </h6>
            
            </div>
            <div class="card-body">
                <div class="col-12">
                  @component('validate.validation')
                  @endcomponent
                </div>
              
                  <form method="POST" action="{{route('user.store')}}" enctype="multipart/form-data"  class="needs-validation" novalidate>
                    @csrf
                 <div class="row">
                    <div class="col-lg-6 col-12">
                         <div class="form-group">
                            <label for="name">{{ __('user.username')}} <sup class="text-danger">*</sup></label>
                            <input type="text" name="username" value="{{old('username')}}" class="form-control" id="username" required>
                            <span class="invalid-feedback">
                                {{trans('setting.the_field_is_required')}}
                              </span>
                         </div>
                         <div class="form-group">
                            <label for="name">{{ __('user.email')}} <sup class="text-danger">*</sup> </label>
                            <input type="email" name="email" value="{{old('email')}}" class="form-control" id="email" required>
                            <span class="invalid-feedback">
                              {{trans('setting.the_field_is_required')}}
                              </span>
                         </div>
                         <div class="form-group">
                            <label for="passord">{{ __('user.password') }}<sup class="text-danger">*</sup></label>
                            <input type="password" class="form-control" name="password"  required id="password">
                            <span class="invalid-feedback">
                              {{trans('setting.the_field_is_required')}}
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">{{ __('user.confirm_password')}} <sup class="text-danger">*</sup></label>
                            <input type="password" class="form-control"  name="password_confirmation" required  id="confirm_password">
                            <span class="confirm-message">
                              
                            </span>
                        </div>
                    </div>
                <div class="col-lg-6 col-12">
                        <div class="form-group">
                            <label for="name">{{ __('user.group')}}</label>
                            <select class="custom-select" name="role_id" id="inputGroupSelect01" data-placeholder="Select a role" style="cursor: pointer;">
                               @foreach($roles as $role)
                               <option value="{{ $role->id }}">{{$role->name }}</option>
                               @endforeach
                              </select>
                        </div>
                        <div class="form-group">
                            <label>Department</label>
                            <select class="select2" multiple="multiple" name="department_id[]" data-placeholder="Select a department" style="width: 100%;">
                                @foreach($departments as $department)
                                <option value="{{ $department->id }}">{{$department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="name">{{__('user.status')}}</label>
                          <select class="custom-select" name="status" id="inputGroupSelect01" style="cursor: pointer;">
                             <option value="1">{{ __('user.active')}}</option>
                             <option value="0">{{ __('user.inactive')}}</option>
                            </select>
                      </div>
                        <label for="photo">{{ __('user.photo')}}</label><br>
                         <div class="custom-file">
                          <input type="file" class="custom-file-input btn btn-primary form-control" id="custom-file-img"  name="photo" accept="image/*">
                          <label class="custom-file-label" for="custom-file-img">{{ __('user.choose_photo')}}</label>
                        </div>
                        <div  class="Size" style="display:none"></div>
                            <div class="form-group"><br>
                            <img src="" id="prev" width="50%" >
                        </div>
                    </div>
                </div>
                    <br>
                    <input type="submit" class="btn btn-success update-btn" value="{{__('user.save')}}">
                  </form>
                </div>
            </div>
          </div>
      </div>
    </div>
  </section>
  @endsection