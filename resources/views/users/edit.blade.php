
@extends('layouts.master')
@section('title')
{{__('user.edit_users')}}
@endsection
@section('content')
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-success card-outline">
            <div class="card-header">
              <h6 class="card-title text-success text-bold">
                <i class="fas fa-user-plus"></i>
                 &nbsp;  &nbsp;<span>{{__('user.add_users')}}</span>
              </h6>
            
            </div>
            <div class="card-body">
                <div class="col-12">
                  @component('validate.validation')
                  @endcomponent
                </div>
              
                  <form method="POST" action="{{route('user.update' , $user->id)}}" enctype="multipart/form-data"  class="needs-validation" novalidate>
                    @csrf
                    @method('PATCH')
                 <div class="row">
                    <div class="col-lg-6 col-12">
                         <div class="form-group">
                            <label for="name">{{ __('user.username')}} <sup class="text-danger">*</sup></label>
                            <input type="text" name="username" value="{{$user->name}}" class="form-control" id="username" required>
                            <span class="invalid-feedback">
                                {{trans('sma.the_field_is_required')}}
                              </span>
                         </div>
                         <div class="form-group">
                            <label for="name">{{ __('user.email')}} <sup class="text-danger">*</sup> </label>
                            <input type="email" name="email" value="{{$user->email}}" class="form-control" id="email" required>
                            <span class="invalid-feedback">
                              {{trans('sma.the_field_is_required')}}
                              </span>
                         </div>
                         <div class="form-group">
                            <label for="passord">{{ __('user.password') }}</label>
                            <input type="password" class="form-control" name="password"  id="password">
                            <small class="text-danger">{{__('setting.keep_it_blank_to_use_the_old_password')}}</small>
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">{{ __('user.confirm_password')}}</label>
                            <input type="password" class="form-control"  name="password_confirmation"  id="confirm_password">
                            <span class="confirm-message">  </span>
                        </div>
                    </div>
                <div class="col-lg-6 col-12">
                  @if(!isOwner())
                        <div class="form-group">
                            <label for="name">{{ __('user.group')}}</label>
                            <select class="custom-select" name="role_id" id="inputGroupSelect01" data-placeholder="Select a role" style="cursor: pointer;">
                               @foreach($roles as $role)
                               <option value="{{ $role->id }}" {{$role->id == $user->role_id ? 'selected' : ''}}>{{$role->name }}</option>
                               @endforeach
                              </select>
                        </div>
                        @else
                        <input type="hidden" name="role_id" value="{{$user->role_id}}">
                        @endif
                        <div class="form-group">
                            <label>Department</label>
                            <select class="select2" multiple="multiple" name="department_id[]" data-placeholder="Select a department" style="width: 100%;">
                              {{-- check if match select multiple --}}
                                @foreach($departments as $department)
                                <option value="{{ $department->id }}" {{in_array($department->id, $user->userDepartments->pluck('id')->toArray()) ? 'selected' : ''}}>{{$department->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="name">{{__('user.status')}}</label>
                          <select class="custom-select" name="status" id="inputGroupSelect01" style="cursor: pointer;">
                            <option value="1" {{$user->status==1 ? 'selected' : ''}}>{{ __('user.active')}}</option>
                             <option value="0" {{$user->status==0 ? 'selected' : ''}}>{{ __('user.inactive')}}</option>
                            </select>
                      </div>
                      <label for="photo">{{ __('user.photo')}}</label><br>
                      <div class="custom-file">
                       <input type="file" class="custom-file-input btn btn-primary form-control" id="custom-file-img"  name="photo" accept="image/*">
                       <label class="custom-file-label" for="custom-file-img">{{ __('user.choose_photo')}}</label>
                     </div>
                     <div  class="Size" style="display:none"></div>
                         <div class="form-group"><br>
                         <img src="{{asset($user->photo ? 'uploads/users/'.$user->photo : 'uploads/users/no_profile.png')}}" id="prev" width="50%" >
                     </div>
                    </div>
                </div>
                    <br>
                    <input type="submit" class="btn btn-success update-btn" value="{{__('user.save')}}">
                  </form>
                </div>
            </div>
          </div>
      </div>
    </div>
  </section>
  @endsection