<?php
 return [
    'book_sale_report' => 'របាយការណ៍លក់សៀវភៅ',
    'reset' => 'កំណត់ឡើងវិញ',
    'date_range' => 'រយៈពេលកាលបរិច្ឆេទ',
    'today' => 'ថ្ងៃនេះ',
    'yesterday' => 'ម្សិលមិញ',
    'last_7_days' => '7 ថ្ងៃចុងក្រោយ',
    'last_30_days' => '30 ថ្ងៃចុងក្រោយ',
    'this_month' => 'ខែនេះ',
    'last_month' => 'ខែមុន',
    'this_year' => 'ឆ្នាំនេះ',
    'last_year' => 'ឆ្នាំមុន',
    'custom_range' => 'រយៈពេលកាលបរិច្ឆេទផ្ទាល់ខ្លួន',
    'course_sale_report' => 'របាយការណ៍លក់វគ្គសិក្សា',
    'course' => 'វគ្គសិក្សា',
    'filter(course_sale)' => 'តម្រង(លក់វគ្គសិក្សា)',
 ]
 ?>