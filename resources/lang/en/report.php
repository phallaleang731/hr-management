<?php
 return [
    'book_sale_report' => 'Book Sale Report',
    'reset' => 'Reset',
    'date_range' => 'Date Range',
    'today' => 'Today',
    'yesterday' => 'Yesterday',
    'last_7_days' => 'Last 7 Days',
    'last_30_days' => 'Last 30 Days',
    'this_month' => 'This Month',
    'last_month' => 'Last Month',
    'this_year' => 'This Year',
    'last_year' => 'Last Year',
    'custom_range' => 'Custom Range',
    'course_sale_report' => 'Course Sale Report',
    'filter(course_sale)' => 'Filter(Course Sale)',
 ]
 ?>