<?php 
 return [
    'can edit role' => 'Can Edit Role',
    'can change permission' => 'Can Change Permission',
    'can delete role' => 'Can Delete Role',
    'can create role' => 'Can Create Role',
    'can view role' => 'Can View Role',
    'can edit department' => 'Can Edit Department',
    'can delete department' => 'Can Delete Department',
    'can create department' => 'Can Create Department',
    'can view department' => 'Can View Department',
    'can assign department' => 'Can Assign Department',
    'can create workflow department' => 'Can Create Workflow Department',
    'can edit workflow department' => 'Can Edit Workflow Department',
    'can approve request' => 'Can Approve Request',
    'can create request' => 'Can Create Request',
    'can view request' => 'Can View Request',
    'can view any request' => 'Can View Any Request',
    'can view user' => 'Can View User',
    'can create user' => 'Can Create User',
    'can edit user' => 'Can Edit User',
    'can view dashboard' => 'Can View Dashboard',
 ];
 ?>